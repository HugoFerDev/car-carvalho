import * as autoridade_action from '../../actions/autoridade';
import * as motorista_action from '../../actions/motorista';

import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
} from 'react-bootstrap';
import React, { Fragment, useEffect } from 'react';

import Page from './page';
import { bindActionCreators } from "redux";
import { compose } from 'redux';
import { connect } from 'react-redux';
import history from '../../utils/history';
import { withFormik } from 'formik';

const Acesso = ({
  setFieldValue,
  handleSubmit,
  handleChange,
  autoridades,
  values,
  motoristas,
  get_motoristas_request,
  get_autoridades_request,
}) => {

  useEffect(() => {
    if (localStorage.getItem('acesso')) history.push('/menu');
    get_autoridades_request();
    get_motoristas_request();
  }, []);

  useEffect(() => {
    const chamadoAtual = JSON.parse(localStorage.getItem('chamado_atual'));
    if (chamadoAtual) {
      history.push('/menu');
    }
  })

  const handleCheck = (item, event) => {
    setFieldValue(item, event.target.value);
  }

  return (
    <Page title="Acesso">
      <div className="d-flex align-items-center mt-4">
        <Container>
          <Form>
            <Row className="d-flex justify-content-center">
              <Col className="col-lg-7">
                <Card>
                  <Card.Body className="shadow rounded">

                    <Form.Group className="d-flex justify-content-around flex-wrap">
                      <div className="d-flex">
                        <div className="round mr-5">
                          <Form.Control
                            type="checkbox"
                            value="autoridade"
                            id="inputAutoridade"
                            className="round"
                            checked={values.tipo === 'autoridade'}
                            name="tipo"
                            onChange={event => handleCheck('tipo', event)}
                          />
                          <Form.Label className="cursor-pointer" htmlFor="inputAutoridade" />
                        </div>
                        <Form.Label className="cursor-pointer" htmlFor="inputAutoridade">Autoridade</Form.Label>
                      </div>

                      <div className="d-flex">
                        <div className="round mr-5">
                          <Form.Control
                            type="checkbox"
                            id="inputMotorista"
                            className="round"
                            value="motorista"
                            name="tipo"
                            checked={values.tipo === 'motorista'}
                            onChange={event => handleCheck('tipo', event)}
                          />
                          <Form.Label className="cursor-pointer" htmlFor="inputMotorista" />
                        </div>
                        <Form.Label className="cursor-pointer" htmlFor="inputMotorista">Motorista</Form.Label>
                      </div>
                    </Form.Group>

                    {
                      values.tipo == 'autoridade'
                        ? (
                          <Fragment>
                            <div className="d-flex w-100">
                              <Form.Group className="d-flex justify-content-between flex-wrap">
                                <Form.Label>Autoridade</Form.Label>
                                <Form.Control as="select" name='autoridade' value={values.autoridade} onChange={handleChange}>
                                  <option>Selecione</option>
                                  {
                                    autoridades.map(autoridade => <option key={autoridade.id} value={autoridade.id}>{autoridade.descricao}</option>)
                                  }
                                </Form.Control>
                              </Form.Group>
                            </div>

                            <div className="d-flex w-100">
                              <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                                <Form.Label>Nome:</Form.Label>
                                <Form.Control
                                  type="text"
                                  className="transform-uppercase"
                                  value={values.nome}
                                  onChange={handleChange}
                                  name="nome"
                                />
                              </Form.Group>
                            </div>
                          </Fragment>
                        )
                        : null
                    }

                    {
                      values.tipo == 'motorista'
                        ? (
                          <div className="d-flex w-100">
                            <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                              <Form.Label>Motorista</Form.Label>
                              <Form.Control as="select" name='motorista' value={values.motorista} onChange={handleChange}>
                                <option>Selecione</option>
                                {
                                  motoristas.map(motorista => <option key={motorista.id} value={motorista.id}>{motorista.nome}</option>)
                                }
                              </Form.Control>
                            </Form.Group>
                          </div>
                        )
                        : null
                    }

                    <Button onClick={handleSubmit} className="w-100 mt-4" variant="primary">
                      Proximo
                    </Button>
                  </Card.Body>
                </Card>

              </Col>
            </Row>
          </Form>
        </Container>
      </div>
    </Page>
  );
}


function mapStateToProps({ autoridade, motorista }) {
  return { autoridades: autoridade.autoridades, motoristas: motorista.motoristas };
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...motorista_action, ...autoridade_action }, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withFormik({
    mapPropsToValues: () => ({ tipo: '', motorista: '', autoridade: '', nome: '' }),

    handleSubmit: async (values, { props }) => {
      localStorage.setItem('acesso', JSON.stringify(values));
      history.push('/menu');
    },
  }),
)(Acesso);
