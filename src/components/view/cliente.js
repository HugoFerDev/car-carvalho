import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import React, { Component } from "react";
import { addCliente, buscarPlaca } from "../../actions/cliente";

import InputMask from "react-input-mask";
import Page from "./page";
import Util from "../../utils/util";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFormik } from "formik";

class Cliente extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    this.setImagem = this.setImagem.bind(this);
  }

  async componentDidMount() {
    const { setFieldValue } = this.props;
    setFieldValue("os", await Util.generateOS());
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue, values } = this.props;

    if (item == "veiculo") {
      const { files } = event.target;
      const imagens = values.imagens.veiculo;
      for (let index = 0; index < files.length; index++) {
        imagens.push(Util.readUploadedFileAsText(files[index]));
      }

      Promise.all(imagens).then((values) => {
        setFieldValue(`imagens.${item}`, values);
      });
    } else {
      const file = event.target.files[0];
      setFieldValue(`imagens.${item}`, await Util.readUploadedFileAsText(file));
    }
  }

  consultarPlaca(value) {
    const { buscarPlaca, setFieldValue } = this.props;
    buscarPlaca(value).then((carro) => {
      setFieldValue("ano", carro.ano);
      setFieldValue("marca", carro.marca);
      setFieldValue("modelo", carro.modelo);
      setFieldValue("chassi", carro.chassi);
      setFieldValue("cor", carro.cor);
    });
  }

  render() {
    const { values, handleChange, handleSubmit } = this.props;
    const { imagens } = values;

    return (
      <Page title="Dados Cliente" pageBack="menu">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card className="min">
                    <Card.Body className="shadow rounded">
                      <Form.Group>
                        <Form.Label>Placa:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.placa}
                          onChange={handleChange}
                          onBlur={() => this.consultarPlaca(values.placa)}
                          name="placa"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Ano:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.ano}
                          onChange={handleChange}
                          name="ano"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Marca:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.marca}
                          onChange={handleChange}
                          name="marca"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Modelo:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.modelo}
                          onChange={handleChange}
                          name="modelo"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Chassi:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.chassi}
                          onChange={handleChange}
                          name="chassi"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Cor:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.cor}
                          onChange={handleChange}
                          name="cor"
                        />
                      </Form.Group>

                      <div className="d-flex align-items-end justify-content-between mt-3">
                        <div className="w-75">
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputFarolDianteiro"
                          >
                            KM:
                          </Form.Label>
                          <Form.Control
                            type="text"
                            className="w-100"
                            id="inputKm"
                            name="km"
                            onChange={handleChange}
                          />
                        </div>
                        <input
                          id="inputImageVelocimetro"
                          style={{ display: "none" }}
                          type="file"
                          name="imagemVelocimetro"
                          accept="image/*"
                          capture
                          onChange={(event) =>
                            this.setImagem("imagemVelocimetro", event)
                          }
                        />
                        <i
                          className="mb-3 fa fa-camera cursor-pointer"
                          onClick={() =>
                            document
                              .getElementById("inputImageVelocimetro")
                              .click()
                          }
                        ></i>
                      </div>

                      <Form.Group>
                        <Form.Label>Origem:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.origem}
                          onChange={handleChange}
                          name="origem"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Remetente:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.remetente}
                          onChange={handleChange}
                          name="remetente"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Endereço Remetente:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.enderecoRemetente}
                          onChange={handleChange}
                          name="enderecoRemetente"
                        />
                      </Form.Group>

                      <Button
                        className="w-100"
                        variant="primary"
                        onClick={handleSubmit}
                      >
                        Proximo
                      </Button>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { cliente: state.cliente };
}

export default compose(
  connect(mapStateToProps, { addCliente, buscarPlaca }),
  withFormik({
    mapPropsToValues: (props) => props.cliente,

    handleSubmit: (values, { props }) => {
      props.addCliente(values);
      props.history.push("/veiculo");
    },
  })
)(Cliente);
