import * as autoridade_action from "../../actions/autoridade";
import * as motorista_action from "../../actions/motorista";
import * as options_action from "../../actions/options";

import React, { Component, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFormik } from "formik";
import { bindActionCreators } from "redux";
import { Container, Row, Col, Button, Card, Form } from "react-bootstrap";
import Page from "./page";
import Util from "../../utils/util";

const NovoChamado = ({
  handleChange,
  handleSubmit,
  create_chamado_request,
  get_patios,
  get_tipo_veiculo,
  setFieldValue,
  values,
  tipoVeiculo,
  patios,
}) => {
  useEffect(() => {
    get_patios();
    get_tipo_veiculo()
  }, []);

  const setImagem = async (item, event) => {
    const imagens = [...values[item]];

    if (imagens.length >= 3) return;

    const file = event.target.files[0];
    imagens.push(await Util.readUploadedFileAsText(file));
    setFieldValue(`${item}`, imagens);
  };

  return (
    <Page title="Dados Cliente" pageBack="menu">
      <div className="d-flex align-items-center mt-4">
        <Container>
          <Form>
            <Row className="d-flex justify-content-center">
              <Col className="col-lg-7">
                <Card className="min">
                  <Card.Body className="shadow rounded">
                    <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                      <Form.Label>Equipamento Solicitado</Form.Label>
                      <Form.Control
                        as="select"
                        name="equipamentoSolicitado"
                        value={values.equipamentoSolicitado}
                        onChange={handleChange}
                      >
                        <option>Selecione</option>
                        <option value="prancha">Prancha</option>
                        <option value="lanca">Lança</option>
                        <option value="munck">Munck</option>
                      </Form.Control>
                    </Form.Group>

                    <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                      <Form.Label>Tipo de Veículo</Form.Label>
                      <Form.Control
                        as="select"
                        name="tipoVeiculo"
                        value={values.tipoVeiculo}
                        onChange={handleChange}
                      >
                        <option>Selecione</option>
                        {tipoVeiculo.map((tipo) => (
                          <option key={tipo.id} value={tipo.id}>
                            {tipo.descricao}
                          </option>
                        ))}
                      </Form.Control>
                    </Form.Group>

                    <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                      <Form.Label>Estado do Veículo</Form.Label>
                      <Form.Control
                        as="select"
                        name="estadoVeiculo"
                        value={values.estadoVeiculo}
                        onChange={handleChange}
                      >
                        <option>Selecione</option>
                        <option value="condicaoNormal">Condição Normal</option>
                        <option value="trancado">Trancado</option>
                        <option value="abandonado">Abandonado</option>
                        <option value="acidentado">Acidentado</option>
                        <option value="capotado">Capotado</option>
                        <option value="travado">Travado</option>
                      </Form.Control>
                    </Form.Group>

                    <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                      <Form.Label>Tipo de Apreensão</Form.Label>
                      <Form.Control
                        as="select"
                        name="tipoapreensao"
                        value={values.tipoapreensao}
                        onChange={handleChange}
                      >
                        <option>Selecione</option>
                        <option value="administrativo">Administrativo</option>
                        <option value="judicial">Judicial</option>
                      </Form.Control>
                    </Form.Group>

                    <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                      <Form.Label>Pátio</Form.Label>
                      <Form.Control
                        as="select"
                        name="patio"
                        value={values.patio}
                        onChange={handleChange}
                      >
                        <option>Selecione</option>
                        {(patios || []).map((tipo) => (
                          <option key={tipo.id} value={tipo.id}>
                            {tipo.nome}
                          </option>
                        ))}
                      </Form.Control>
                    </Form.Group>

                    <div className="d-flex align-items-end justify-content-between mt-3">
                      <div className="w-90">
                        <Form.Label
                          className="cursor-pointer"
                          htmlFor="inputFarolDianteiro"
                        >
                          Local do Chamado: {values.imagemLocal.length}/3
                        </Form.Label>
                        <Form.Control
                          onChange={handleChange}
                          type="text"
                          name="localChamado"
                          placeholder="Local do Chamado"
                        />
                      </div>
                      <input
                        id="inputImageLocal"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemLocal"
                        accept="image/*"
                        capture
                        onChange={(event) => setImagem("imagemLocal", event)}
                      />
                      <i
                        className="mb-3 fa fa-camera cursor-pointer"
                        onClick={() =>
                          document.getElementById("inputImageLocal").click()
                        }
                      ></i>
                    </div>

                    <Form.Group className="d-flex w-100 justify-content-between flex-wrap">
                      <Form.Label>Quantidade:</Form.Label>
                      <Form.Control
                        type="text"
                        className="transform-uppercase"
                        value={values.qtde}
                        onChange={handleChange}
                        name="qtde"
                      />
                    </Form.Group>

                    <Button
                      className="w-100 mt-4"
                      variant="primary"
                      onClick={handleSubmit}
                    >
                      Abrir Chamado
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Form>
        </Container>
      </div>
    </Page>
  );
};

function mapStateToProps(state) {
  return {
    cliente: state.cliente,
    tipoVeiculo: state.options.tipoVeiculo,
    patios: state.options.patios,
  };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    { ...motorista_action, ...autoridade_action, ...options_action },
    dispatch
  );

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withFormik({
    mapPropsToValues: () => ({
      equipamentoSolicitado: "",
      estadoVeiculo: "",
      tipoVeiculo: "",
      patio: "",
      localChamado: "",
      tipoapreensao: "",
      qtde: 1,
      imagemLocal: [],
    }),

    handleSubmit: (values, { props }) => {
      const acesso = JSON.parse(localStorage.getItem("acesso")) || {};

      navigator.geolocation.getCurrentPosition(
        ({ coords: { latitude, longitude } }) => {
          props
            .create_chamado_request({
              ...values,
              latitude,
              longitude,
              idchamador: parseInt(acesso.autoridade),
              nome: acesso.nome,
              tipoChamador: props.match.params.tipo,
            })
            .then(() => {
              props.history.push("/menu");
            });
        },
        () => {},
        {
          timeout: 3000,
          enableHighAccuracy: true,
        }
      );
    },
  })
)(NovoChamado);
