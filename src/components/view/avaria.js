import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
  Spinner,
} from 'react-bootstrap';
import React, { Component } from 'react';

import Page from './page';
import Util from '../../utils/util';
import { addAvaria } from '../../actions/avaria';
import { saveCheckList } from '../../actions/check-list';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';

class Avaria extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {};
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue, values } = this.props;
    const { files } = event.target;
    const imagens = [];
    for (let index = 0; index < files.length; index++) {
      imagens.push(Util.readUploadedFileAsText(files[index]));
    }

    Promise.all(imagens).then((data) => {
      const res = values[item];
      if (res.length >= 10) return;
      data.forEach(element => {
        res.push(element);
      });

      setFieldValue(`${item}`, res);
    })
  }

  render() {
    const { values, handleChange, handleSubmit, avaria } = this.props;
    const { imagemLateralDireita, imagemLateralEsquerda, imagemTraseira, imagemDianteira, imagemTeto, imagemOutros } = values;

    if (avaria.loading) {
      return (
        <div
          className="d-flex align-items-center justify-content-center"
          style={{ height: '100vh' }}
        >
          <div className="p-4 rounded" style={{ backgroundColor: '#fff' }}>
            <Spinner animation="border" variant="primary" />
          </div>
        </div>
      );
    }
    return (
      <Page title="AVARIAS" pageBack="complemento">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card>
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title> Lateral Direita {`(${imagemLateralDireita.length}/10)`}</Card.Title>
                        <i
                          style={{ fontSize: '25px' }}
                          onClick={() => document.getElementById('imagemLateralDireita').click()}
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemLateralDireita"
                        style={{ display: 'none' }}
                        type="file"
                        name="imagemLateralDireita"
                        accept="image/*"
                        capture
                        onChange={event => this.setImagem('imagemLateralDireita', event)}
                      />

                      <Form.Group>
                        <Form.Label>Descreva a Avaria:</Form.Label>
                        <Form.Control
                          as="textarea"
                          className="transform-uppercase"
                          value={values.descricaoLateralDireita}
                          onChange={handleChange}
                          name="descricaoLateralDireita"
                        />
                      </Form.Group>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title> Lateral Esquerda {`(${imagemLateralEsquerda.length}/10)`}</Card.Title>
                        <i
                          style={{ fontSize: '25px' }}
                          onClick={() => document.getElementById('imagemLateralEsquerda').click()}
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemLateralEsquerda"
                        style={{ display: 'none' }}
                        type="file"
                        name="imagemLateralEsquerda"
                        accept="image/*"
                        capture
                        onChange={event => this.setImagem('imagemLateralEsquerda', event)}
                      />

                      <Form.Group>
                        <Form.Label>Descreva a Avaria:</Form.Label>
                        <Form.Control
                          as="textarea"
                          className="transform-uppercase"
                          value={values.descricaoLateralEsquerda}
                          onChange={handleChange}
                          name="descricaoLateralEsquerda"
                        />
                      </Form.Group>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title> Traseira {`(${imagemTraseira.length}/10)`}</Card.Title>
                        <i
                          style={{ fontSize: '25px' }}
                          onClick={() => document.getElementById('imagemTraseira').click()}
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemTraseira"
                        style={{ display: 'none' }}
                        type="file"
                        name="imagemTraseira"
                        accept="image/*"
                        capture
                        onChange={event => this.setImagem('imagemTraseira', event)}
                      />

                      <Form.Group>
                        <Form.Label>Descreva a Avaria:</Form.Label>
                        <Form.Control
                          as="textarea"
                          className="transform-uppercase"
                          value={values.descricaoTraseira}
                          onChange={handleChange}
                          name="descricaoTraseira"
                        />
                      </Form.Group>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title> Dianteira {`(${imagemDianteira.length}/10)`}</Card.Title>
                        <i
                          style={{ fontSize: '25px' }}
                          onClick={() => document.getElementById('imagemDianteira').click()}
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemDianteira"
                        style={{ display: 'none' }}
                        type="file"
                        name="imagemDianteira"
                        accept="image/*"
                        capture
                        onChange={event => this.setImagem('imagemDianteira', event)}
                      />

                      <Form.Group>
                        <Form.Label>Descreva a Avaria:</Form.Label>
                        <Form.Control
                          as="textarea"
                          className="transform-uppercase"
                          value={values.descricaoDianteira}
                          onChange={handleChange}
                          name="descricaoDianteira"
                        />
                      </Form.Group>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title> Teto {`(${imagemTeto.length}/10)`}</Card.Title>
                        <i
                          style={{ fontSize: '25px' }}
                          onClick={() => document.getElementById('imagemTeto').click()}
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemTeto"
                        style={{ display: 'none' }}
                        type="file"
                        name="imagemTeto"
                        accept="image/*"
                        capture
                        onChange={event => this.setImagem('imagemTeto', event)}
                      />

                      <Form.Group>
                        <Form.Label>Descreva a Avaria:</Form.Label>
                        <Form.Control
                          as="textarea"
                          className="transform-uppercase"
                          value={values.descricaoTeto}
                          onChange={handleChange}
                          name="descricaoTeto"
                        />
                      </Form.Group>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title> Outros {`(${imagemOutros.length}/10)`}</Card.Title>
                        <i
                          style={{ fontSize: '25px' }}
                          onClick={() => document.getElementById('imagemOutros').click()}
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemOutros"
                        style={{ display: 'none' }}
                        type="file"
                        name="imagemOutros"
                        accept="image/*"
                        capture
                        onChange={event => this.setImagem('imagemOutros', event)}
                      />

                      <Form.Group>
                        <Form.Label>Descreva a Avaria:</Form.Label>
                        <Form.Control
                          as="textarea"
                          className="transform-uppercase"
                          value={values.descricaoOutros}
                          onChange={handleChange}
                          name="descricaoOutros"
                        />
                      </Form.Group>


                      <Button onClick={handleSubmit} className="w-100 mt-4" variant="primary">
                        Gravar Checklist
                      </Button>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { avaria: state.avaria, check: state };
}

export default compose(
  connect(
    mapStateToProps,
    { addAvaria, saveCheckList },
  ),
  withFormik({
    mapPropsToValues: props => props.avaria,

    handleSubmit: (values, { props }) => {
      props.addAvaria(values);
      let check = props.check;
      check = Object.assign({ ...check, cliente: { ...check.cliente, chamadoId: localStorage.getItem('chamado_id') } }, { avaria: values });
      props.saveCheckList(check).then(() => {
        props.history.push('/menu');
      })
    },
  }),
)(Avaria);
