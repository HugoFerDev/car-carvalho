import * as autoridade_action from '../../actions/autoridade';
import * as motorista_action from '../../actions/motorista';

import {
  Button,
  Container,
  Form,
  Row,
} from 'react-bootstrap';
import React, { Fragment, useEffect } from 'react';

import AutoridadeMenu from './autoridade-menu';
import ChamadoAtual from './chamado-atual';
import Page from './page';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import history from '../../utils/history';

const Menu = ({ loading_autoridade, success_autoridade, create_chamado_request, reset_autoridade }) => {

  useEffect(() => {
    if (!loading_autoridade && success_autoridade) {
      let chamado = JSON.parse(localStorage.getItem('chamado'));
      reset_autoridade()
      history.push(`/chamado/${chamado.insertId}`);
    }
  }, [loading_autoridade, success_autoridade])

  const acesso = JSON.parse(localStorage.getItem('acesso')) || {};

  return (
    <Page title="OPÇÕES" pageBack=''>
      <div className='d-flex flex-column justify-content-between h-80vh'>
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">

                {
                  acesso.tipo === 'motorista' ? (
                    <Fragment>
                      <Button onClick={() => history.push('/novo-chamado/motorista')} className="w-100 mt-3" variant="primary">
                        NOVO CHAMADO
                      </Button>

                      <Button onClick={() => history.push(`/options-chamados/${parseInt(acesso.motorista)}`)} className="w-100 mt-3" variant="primary">
                        CHAMADOS
                      </Button>
                    </Fragment>

                  ) : null
                }

                {
                  acesso.tipo === 'autoridade' ? (
                    <Fragment>
                      <Button onClick={() => history.push('/novo-chamado/autoridade')} className="w-100 mt-3" variant="primary">
                        NOVO CHAMADO
                      </Button>
                      <AutoridadeMenu />
                    </Fragment>
                  ) : null
                }

              </Row>
            </Form>
          </Container>
        </div>
        <ChamadoAtual />
      </div>
    </Page>
  )
}

function mapStateToProps({ autoridade }) {
  return {
    loading_autoridade: autoridade.loading,
    success_autoridade: autoridade.success,
  };
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...motorista_action, ...autoridade_action }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
