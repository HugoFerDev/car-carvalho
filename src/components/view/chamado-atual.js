import * as React from "react";
import * as chamado_action from "../../actions/chamado";

import { Fragment, useEffect } from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import enviroment from "../../enviroment";

const ChamadoAtual = ({
  chamados,
  update_location_chamado_request,
  get_chamados_by_motorista_request
}) => {
  const chamadoAtual = JSON.parse(localStorage.getItem("chamado_atual"));
  const acesso = JSON.parse(localStorage.getItem("acesso"));

  useEffect(() => {
    // get_chamados_by_motorista_request(acesso.motorista);
  }, []);

  function update() {
    // navigator.geolocation.getCurrentPosition(
    //   ({ coords: { latitude, longitude } }) => {
    //     update_location_chamado_request({ idchamado: chamadoAtual.id, latitude: latitude, longitude: longitude });
    //   },
    //   () => { },
    //   {
    //     timeout: 3000,
    //     enableHighAccuracy: true,
    //   }
    // )
  }

  useEffect(() => {
    const cAtual = chamados.filter(chamado => chamado.status)[0];
    localStorage.removeItem("chamado_atual");
    if (cAtual) {
      localStorage.setItem(
        "chamado_atual",
        JSON.stringify({ ...cAtual, chamado: cAtual })
      );
    }
  }, [chamados]);

  useEffect(() => {
    if (chamadoAtual) {
      const interval = setInterval(() => {
        update();
      }, enviroment.TIME_REFRESH);
      return () => clearInterval(interval);
    }
  }, []);

  return <Fragment />;
};

function mapStateToProps({ chamado }) {
  return {
    chamados: chamado.chamados
  };
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...chamado_action }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChamadoAtual);
