import {
  Button
} from 'react-bootstrap';
import React, { Component } from 'react';

import Page from './page';

import 'leaflet-offline';
import localforage from 'localforage';

import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import L from 'leaflet';
class Guincho extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {
      region: {
        lat: 59.95,
        lng: 30.33
      },
      zoom: 18,
    }
  }

  checkLocation() {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        this.setState({
          region: {
            lat: latitude,
            lng: longitude
          }
        });
      },
      () => { },
      {
        timeout: 3000,
        enableHighAccuracy: true,
      }
    )
  }

  componentDidMount() {
    this.checkLocation();
    const map = L.map('map-id');
    const offlineLayer = L.tileLayer.offline('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', localforage, {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      subdomains: 'abc',
      minZoom: 10,
      maxZoom: 20,
      crossOrigin: true
    });
    offlineLayer.addTo(map);
  }

  render() {
    const { region } = this.state;
    return (
      <Page title="AVARIAS" pageBack="menu">
        <div>
          <div id="map-id">
            <Map center={region} zoom={this.state.zoom} maxZoom={30}
              id="map">
              <TileLayer
                attribution="&copy; <a href=&quot;https://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
              />
              <Marker position={region}></Marker>
            </Map>
          </div>
          {/* <LeafletMap center={region} zoom={this.state.zoom}>
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
            />
            <Marker position={region}>
              <Popup>
                A pretty CSS3 popup. <br /> Easily customizable.
          </Popup>
            </Marker>
          </LeafletMap> */}
        </div>

      </Page >
    );
  }
}

export default Guincho
