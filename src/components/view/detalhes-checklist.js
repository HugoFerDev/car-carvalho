import {
  Container, Row, Image, Col,
} from 'react-bootstrap';
import React from 'react';

const DetalheChecklist = ({ checklist }) => (
  <Container className="overflow-auto max-h-26">

    <div>
      <h5>Cliente</h5>

      <div className="d-flex justify-content-around flex-wrap">
        <div className="d-flex flex-column">

          <div>
            <span>Placa:</span>
            <h6>{checklist.cliente.placa}</h6>
          </div>

          <div>
            <span>Remetente:</span>
            <h6>{checklist.cliente.remetente}</h6>
          </div>

          <div>
            <span>Endereço do Remetente:</span>
            <h6>{checklist.cliente.enderecoRemetente}</h6>
          </div>

          <div>
            <span>Telefone do Remetente:</span>
            <h6>{checklist.cliente.telefoneRemetente}</h6>
          </div>

        </div>

        <div className="d-flex flex-column">
          <div>
            <span>Destinatário:</span>
            <h6>{checklist.cliente.destinatario}</h6>
          </div>

          <div>
            <span>Marca:</span>
            <h6>{checklist.cliente.marca}</h6>
          </div>

          <div>
            <span>Endereço do Destinatário:</span>
            <h6>{checklist.cliente.enderecoDestinatario}</h6>
          </div>

          <div>
            <span>Telefone do Destinatário:</span>
            <h6>{checklist.cliente.telefoneDestinatario}</h6>
          </div>

        </div>
      </div>
    </div>

    <div className="divider" />

    <div>
      <h5>Veículo</h5>

      <div className="d-flex justify-content-around flex-wrap">
        <div className="d-flex flex-column">
          <div>
            <span>Acendedor:</span>
            <h6>{checklist.veiculo.acendedor ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Antena:</span>
            <h6>{checklist.veiculo.antena ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Calotas:</span>
            <h6>{checklist.veiculo.calotas ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Carro Funcionando:</span>
            <h6>{checklist.veiculo.carroFuncionando ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Chave Reserva:</span>
            <h6>{checklist.veiculo.chaveReserva ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Chave Roda:</span>
            <h6>{checklist.veiculo.chaveRoda ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Documento:</span>
            <h6>{checklist.veiculo.documento ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>DVD:</span>
            <h6>{checklist.veiculo.dvd ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Extintor:</span>
            <h6>{checklist.veiculo.extintor ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Frente:</span>
            <h6>{checklist.veiculo.frente ? 'Sim' : 'Não'}</h6>
          </div>

        </div>

        <div className="d-flex flex-column">
          <div>
            <span>Macaco:</span>
            <h6>{checklist.veiculo.macaco ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Manual:</span>
            <h6>{checklist.veiculo.manual ? 'Sim' : 'Não'}</h6>
          </div>
          <div>
            <span>Pneu de Step:</span>
            <h6>{checklist.veiculo.pneuStep ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Radio CD:</span>
            <h6>{checklist.veiculo.radioCd ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Roda Comum:</span>
            <h6>{checklist.veiculo.rodaComum ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Roda Especial:</span>
            <h6>{checklist.veiculo.rodaEspecial ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Tapete:</span>
            <h6>{checklist.veiculo.tapete ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Tela:</span>
            <h6>{checklist.veiculo.tela ? 'Sim' : 'Não'}</h6>
          </div>

          <div>
            <span>Triangulo:</span>
            <h6>{checklist.veiculo.triangulo ? 'Sim' : 'Não'}</h6>
          </div>
        </div>
      </div>
    </div>

    <div className="divider" />

    <div>
      <h5>Complemento</h5>

      <div>
        <span>Marca da Bateria:</span>
        <h6>{checklist.complemento.marcaBateria}</h6>
      </div>

      <div>
        <span>Pintura:</span>
        <h6>{checklist.complemento.pintura}</h6>
      </div>

      <div>
        <span>Pneus:</span>
        <h6>{checklist.complemento.pneus}</h6>
      </div>

      <div>
        <h6>Tapeçaria:</h6>
        <span>{checklist.complemento.tapecaria}</span>
      </div>
    </div>

    <div className="divider" />

    <div>
      <h5>Avaria</h5>

      <div>
        <h6>Lateral Direita</h6>
        <Row className="d-flex flex-nowrap flex-row overflow-auto p-3 m-2">

          {
            checklist.avaria.imagemLateralDireita.map(lateral => <Col className="d-flex align-items-center flex-column min-w-14">
              <Image className="img-thumbnail w-rem-12" src={lateral} />
            </Col>)
          }

        </Row>
        <div>
          <h6>Descrição:</h6>
          <span>{checklist.avaria.descricaoLateralDireita}</span>
        </div>
      </div>

      <div className="divider" />

      <div>
        <h6>Lateral Esquerda</h6>
        <Row className="d-flex flex-nowrap flex-row overflow-auto p-3 m-2">

          {
            checklist.avaria.imagemLateralEsquerda.map(lateral => <Col className="d-flex align-items-center flex-column min-w-14">
              <Image className="img-thumbnail w-rem-12" src={lateral} />
            </Col>)
          }

        </Row>
        <div>
          <h6>Descrição:</h6>
          <span>{checklist.avaria.descricaoLateralEsquerda}</span>
        </div>
      </div>

      <div className="divider" />

      <div>
        <h6>Traseira</h6>
        <Row className="d-flex flex-nowrap flex-row overflow-auto p-3 m-2">

          {
            checklist.avaria.imagemTraseira.map(lateral => <Col className="d-flex align-items-center flex-column min-w-14">
              <Image className="img-thumbnail w-rem-12" src={lateral} />
            </Col>)
          }

        </Row>
        <div>
          <h6>Descrição:</h6>
          <span>{checklist.avaria.descricaoTraseira}</span>
        </div>
      </div>

      <div className="divider" />

      <div>
        <h6>Dianteira</h6>
        <Row className="d-flex flex-nowrap flex-row overflow-auto p-3 m-2">

          {
            checklist.avaria.imagemDianteira.map(lateral => <Col className="d-flex align-items-center flex-column min-w-14">
              <Image className="img-thumbnail w-rem-12" src={lateral} />
            </Col>)
          }

        </Row>
        <div>
          <h6>Descrição:</h6>
          <span>{checklist.avaria.descricaoimagemDianteira}</span>
        </div>
      </div>

      <div className="divider" />

      <div>
        <h6>Teto</h6>
        <Row className="d-flex flex-nowrap flex-row overflow-auto p-3 m-2">

          {
            checklist.avaria.imagemTeto.map(lateral => <Col className="d-flex align-items-center flex-column min-w-14">
              <Image className="img-thumbnail w-rem-12" src={lateral} />
            </Col>)
          }

        </Row>
        <div>
          <h6>Descrição:</h6>
          <span>{checklist.avaria.descricaoTeto}</span>
        </div>
      </div>

      <div className="divider" />

      <div>
        <h6>Outros</h6>
        <Row className="d-flex flex-nowrap flex-row overflow-auto p-3 m-2">

          {
            checklist.avaria.imagemOutros.map(lateral => <Col className="d-flex align-items-center flex-column min-w-14">
              <Image className="img-thumbnail w-rem-12" src={lateral} />
            </Col>)
          }

        </Row>
        <div>
          <h6>Descrição:</h6>
          <span>{checklist.avaria.descricaoOutros}</span>
        </div>
      </div>

    </div>

  </Container>
);

export default DetalheChecklist;
