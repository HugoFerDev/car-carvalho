import { compose } from 'redux';
import { withFormik } from 'formik';
import { connect } from 'react-redux';
import React, { Component } from 'react';

import {
  Container, Row, Col, Card, Form, Button,
} from 'react-bootstrap';
import Page from './page';
import { addVeiculo } from '../../actions/veiculo';
import Util from '../../utils/util';

class Veiculo extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {};
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue } = this.props;

    const file = event.target.files[0];
    setFieldValue(`${item}`, await Util.readUploadedFileAsText(file));
  }

  render() {
    const { values, handleChange, handleSubmit } = this.props;

    return (
      <Page title="ACESSÓRIOS" pageBack="cliente">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card>
                    <Card.Body className="shadow rounded">

                      <div className="d-flex justify-content-between flex-wrap">

                        <div className="d-flex flex-column">

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputAscendedor"
                                onChange={handleChange}
                                name="acendedor"
                                checked={values.acendedor}
                              />
                              <Form.Label htmlFor="inputAscendedor" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputAscendedor">Ascendedor</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputRadioCd"
                                onChange={handleChange}
                                name="radioCd"
                                checked={values.radioCd}
                              />
                              <Form.Label htmlFor="inputRadioCd" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputRadioCd">inputRadio CD</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputFrente"
                                onChange={handleChange}
                                name="frente"
                                checked={values.frente}
                              />
                              <Form.Label htmlFor="inputFrente" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputFrente">Frente</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputTela"
                                onChange={handleChange}
                                name="tela"
                                checked={values.tela}
                              />
                              <Form.Label htmlFor="inputTela" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputTela">Tela</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputDvd"
                                onChange={handleChange}
                                name="dvd"
                                checked={values.dvd}
                              />
                              <Form.Label htmlFor="inputDvd" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputDvd">DVD</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputTapete"
                                onChange={handleChange}
                                name="tapete"
                                checked={values.tapete}
                              />
                              <Form.Label htmlFor="inputTapete" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputTapete">Tapetes</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputExtintor"
                                onChange={handleChange}
                                name="extintor"
                                checked={values.extintor}
                              />
                              <Form.Label htmlFor="inputExtintor" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputExtintor">Extintor</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputManual"
                                onChange={handleChange}
                                name="manual"
                                checked={values.manual}
                              />
                              <Form.Label htmlFor="inputManual" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputManual">Manual</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputChaveReserva"
                                onChange={handleChange}
                                name="chaveReserva"
                                checked={values.chaveReserva}
                              />
                              <Form.Label htmlFor="inputChaveReserva" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputChaveReserva">Chave Reserva</Form.Label>
                          </div>
                        </div>

                        <div className="d-flex flex-column">
                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputPneuStep"
                                onChange={handleChange}
                                name="pneuStep"
                                checked={values.pneuStep}
                              />
                              <Form.Label htmlFor="inputPneuStep" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputPneuStep">Pneu Step</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputMacaco"
                                onChange={handleChange}
                                name="macaco"
                                checked={values.macaco}
                              />
                              <Form.Label htmlFor="inputMacaco" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputMacaco">Macaco</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputTriangulo"
                                onChange={handleChange}
                                name="triangulo"
                                checked={values.triangulo}
                              />
                              <Form.Label htmlFor="inputTriangulo" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputTriangulo">Triangulo</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputChaveRoda"
                                onChange={handleChange}
                                name="chaveRoda"
                                checked={values.chaveRoda}
                              />
                              <Form.Label htmlFor="inputChaveRoda" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputChaveRoda">Chave de Roda</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputRodaComum"
                                onChange={handleChange}
                                name="rodaComum"
                                checked={values.rodaComum}
                              />
                              <Form.Label htmlFor="inputRodaComum" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputRodaComum">Roda Comum</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputRodaEspecial"
                                onChange={handleChange}
                                name="rodaEspecial"
                                checked={values.rodaEspecial}
                              />
                              <Form.Label htmlFor="inputRodaEspecial" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputRodaEspecial">Roda Especial</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputCalotas"
                                onChange={handleChange}
                                name="calotas"
                                checked={values.calotas}
                              />
                              <Form.Label htmlFor="inputCalotas" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputCalotas">Calotas</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputAntena"
                                onChange={handleChange}
                                name="antena"
                                checked={values.antena}
                              />
                              <Form.Label htmlFor="inputAntena" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputAntena">Antena</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputDocumento"
                                onChange={handleChange}
                                name="documento"
                                checked={values.documento}
                              />
                              <Form.Label htmlFor="inputDocumento" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputDocumento">Documento</Form.Label>
                          </div>

                          <div className="d-flex">
                            <div className="round mr-5">
                              <Form.Control
                                type="checkbox"
                                className="round"
                                id="inputCarroFuncionando"
                                onChange={handleChange}
                                name="carroFuncionando"
                                checked={values.carroFuncionando}
                              />
                              <Form.Label htmlFor="inputCarroFuncionando" />
                            </div>
                            <Form.Label className="cursor-pointer" htmlFor="inputCarroFuncionando">Carro Funcionando</Form.Label>
                          </div>
                        </div>
                      </div>

                      <Button onClick={handleSubmit} className="w-100 mt-4" variant="primary">
                        Proximo
                      </Button>
                    </Card.Body>
                  </Card>

                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { motor: state.motor };
}

export default compose(
  connect(
    mapStateToProps,
    { addVeiculo },
  ),
  withFormik({
    mapPropsToValues: props => props.motor,

    handleSubmit: (values, { props }) => {
      props.addVeiculo(values);
      props.history.push('/complemento');
    },
  }),
)(Veiculo);
