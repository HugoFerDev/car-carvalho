import * as chamado_action from "../../actions/chamado";

import React, { useEffect } from "react";
import {
  Table,
  TableCell,
  TableContainer,
  TableHeaderCell,
  TableHeaderRow,
  TableRow,
} from "../styles/Table";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import history from "../../utils/history";
import moment from "moment";

const AuridadeMenu = ({ chamados, get_chamados_by_autoridade_request }) => {
  useEffect(() => {
    let acesso = JSON.parse(localStorage.getItem("acesso"));
    get_chamados_by_autoridade_request(acesso.autoridade, acesso.nome);
  }, []);

  console.log(chamados);
  return (
    <div style={{ width: "100%", marginTop: "2em" }}>
      <TableContainer>
        <Table>
          <TableHeaderRow>
            <TableHeaderCell>NCV</TableHeaderCell>
            <TableHeaderCell>Chamador</TableHeaderCell>
            <TableHeaderCell>Motorista</TableHeaderCell>
            <TableHeaderCell>Data</TableHeaderCell>
            <TableHeaderCell>Endereço</TableHeaderCell>
            <TableHeaderCell>Ver Mapa</TableHeaderCell>
          </TableHeaderRow>
          {chamados.map((chamado, index) => (
            <TableRow
              style={{
                background: chamado.status && "#32a852",
                color: chamado.status && "#fff",
              }}
              key={chamado.id}
            >
              <TableCell data-title="NCV">{chamado.ncv}</TableCell>
              <TableCell data-title="Chamador">
                {chamado.nome_chamador}
              </TableCell>
              <TableCell data-title="Motorista" className="transform-uppercase">
                {chamado.nome_motorista}
              </TableCell>
              <TableCell data-title="Data">
                {moment(chamado.data_hora_chamador).format("DD/MM/YYYY HH:mm")}
              </TableCell>
              <TableCell data-title="Endereço">
                {chamado.endereco &&
                  chamado.endereco.address &&
                  `${chamado.endereco.address.road}, ${chamado.endereco.address.city_district}, ${chamado.endereco.address.state}`}
              </TableCell>
              <TableCell data-title="Ações">
                <div className="d-flex justify-content-center">
                  <i
                    className="fa fa-search cursor-pointer"
                    onClick={() => history.push(`/chamado/${chamado.id}`)}
                  />
                </div>
              </TableCell>
            </TableRow>
          ))}
        </Table>
      </TableContainer>
    </div>
  );
};

function mapStateToProps({ chamado }) {
  return {
    chamados: chamado.chamados,
  };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...chamado_action }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AuridadeMenu);
