const initialState = {
  loading: false,
  autoridades: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS': {
      return {
        ...state,
        autoridades: action.response,
      };
    }

    default: {
      return state;
    }
  }
};
