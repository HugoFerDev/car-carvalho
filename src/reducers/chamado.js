const initialState = {
  loading: false,
  chamados: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_CHAMADO_SUCCESS': {
      return {
        ...state,
        chamados: action.response,
      };
    }

    default: {
      return state;
    }
  }
};
