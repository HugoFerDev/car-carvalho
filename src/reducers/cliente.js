import { ADD_CLIENTE, BUSCAR_PLACA, RESET_STATE } from '../actions/types';

const initialState = {
  os: '',
  km: '',
  ano: '',
  cor: '',
  placa: '',
  marca: '',
  modelo: '',
  chassi: '',
  origem: '',

  remetente: '',
  enderecoRemetente: '',
  telefoneRemetente: '',

  destinatario: '',
  telefoneDestinatario: '',
  enderecoDestinatario: '',

  imagemVelocimetro: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CLIENTE: {
      const { cliente } = action;
      return Object.assign({}, state, cliente);
    }

    case RESET_STATE: {
      return initialState;
    }

    case BUSCAR_PLACA: {
      const { resposta } = action;
      return {
        ...state,
        chassi: resposta.chassi,
        anoModelo: resposta.anoModelo,
      };
    }

    default: {
      return state;
    }
  }
};
