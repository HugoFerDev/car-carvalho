import { FETCH_CHECKLIST } from '../actions/types';

const initialState = {
  checklists: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CHECKLIST: {
      const { response } = action;
      return { ...state, checklists: response };
    }

    default: {
      return state;
    }
  }
};
