import { ADD_COMPLEMENTO, RESET_STATE } from '../actions/types';

const initialState = {
  pneus: '',
  pintura: '',
  tapecaria: '',
  combustivel: '',
  marcaBateria: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_COMPLEMENTO: {
      const { complemento } = action;
      return Object.assign({}, state, complemento);
    }

    case RESET_STATE: {
      return initialState;
    }

    default: {
      return state;
    }
  }
};
