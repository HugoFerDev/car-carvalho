import { FETCH_TIPO_VEICULO, FETCH_PATIOS_VEICULO } from "../actions/types";

const initialState = {
  tipoVeiculo: [],
  patios: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TIPO_VEICULO: {
      const { response } = action;
      return { ...state, tipoVeiculo: response };
    }
    case FETCH_PATIOS_VEICULO: {
      const { response } = action;
      return { ...state, patios: response };
    }
    default: {
      return state;
    }
  }
};
