import { ADD_AVARIA, RESET_STATE } from '../actions/types';

const initialState = {
  descricaoLateralDireita: '',
  imagemLateralDireita: [],

  descricaoLateralEsquerda: '',
  imagemLateralEsquerda: [],

  descricaoTraseira: '',
  imagemTraseira: [],

  descricaoDianteira: '',
  imagemDianteira: [],

  descricaoTeto: '',
  imagemTeto: [],

  descricaoOutros: '',
  imagemOutros: [],

  observacao: '',
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_AVARIA: {
      const { avaria } = action;
      return Object.assign({}, state, { ...avaria, loading: true });
    }

    case RESET_STATE: {
      return initialState;
    }

    default: {
      return state;
    }
  }
};
