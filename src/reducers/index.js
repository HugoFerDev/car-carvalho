import autoridade from './autoridades';
import avaria from './avaria';
import chamado from './chamado';
import checklist from './checklist';
import cliente from './cliente';
import { combineReducers } from 'redux';
import complemento from './complemento';
import motorista from './motorista';
import veiculo from './veiculo';
import options from './options';

export default combineReducers({
  veiculo,
  options,
  cliente,
  avaria,
  complemento,
  checklist,
  autoridade,
  motorista,
  chamado,
});
