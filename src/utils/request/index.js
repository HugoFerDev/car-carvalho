export default (url, method, body) => {
  return fetch(url, {
    method: method || 'GET',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      Authorization: JSON.parse(localStorage.getItem('acesso')).token,
      'pragma': 'no-cache',
      'cache-control': 'no-cache'
    }
  })
}
