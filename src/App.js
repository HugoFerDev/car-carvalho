import { Redirect, Route, Router, Switch } from "react-router-dom";

import Avaria from "./components/view/avaria";
import Chamado from "./components/view/chamado";
import Cliente from "./components/view/cliente";
import Complemento from "./components/view/complemento";
import Guincho from "./components/view/guincho";
import Menu from "./components/view/menu";
import OptionChamados from "./components/view/option-chamados";
import NovoChamado from "./components/view/novo-chamado";
import { Provider } from "react-redux";
import React, { useState, useEffect } from "react";
import Relatorio from "./components/view/relatorio";
import Veiculo from "./components/view/veiculo";
import configureStore from "./store";
import history from "./utils/history";
import Login from "./components/view/login";
import { Button, Modal } from "react-bootstrap";

const store = configureStore();
let installPrompt = null;

const PrivateRoute = ({ ...props }) =>
  localStorage.getItem("acesso") ? <Route {...props} /> : <Redirect to="/" />;

const App = () => {
  const [hideInstall, setHideInstall] = useState(false);

  const installApp = async () => {
    if (!installPrompt) return false;
    installPrompt.prompt();
    let outcome = await installPrompt.userChoice;
    if (outcome.outcome == "accepted") {
      console.log("App Installed");
    } else {
      console.log("App not installed");
    }
    installPrompt = null;
    setHideInstall(false);
  };

  const handleClose = () => {
    setHideInstall(false);
  };

  useEffect(() => {
    window.addEventListener("beforeinstallprompt", (e) => {
      e.preventDefault();
      installPrompt = e;
      if (
        (window.matchMedia &&
          window.matchMedia("(display-mode: standalone)").matches) ||
        window.navigator.standalone === true
      ) {
        return false;
      }

      setHideInstall(true);
    });
  }, []);

  return (
    <Provider store={store}>
      <Router history={history}>
        <div>
          <Modal show={hideInstall} onHide={handleClose} size="lg">
            <Modal.Header closeButton>
              <Modal.Title>Instalar Aplicativo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="d-flex justify-content-center flex-column align-items-center">
                <img
                  alt="Logo"
                  width="100"
                  src={require("./assets/img/icon-logo.jpeg")}
                />
                <p>Deseja instalar o app?</p>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Cancelar
              </Button>

              <Button variant="primary" onClick={installApp}>
                Instalar
              </Button>
            </Modal.Footer>
          </Modal>

          <Switch>
            <Route path="/" exact component={Login} />

            <PrivateRoute path="/menu" component={Menu} />
            <PrivateRoute path="/veiculo" component={Veiculo} />
            <PrivateRoute path="/cliente" component={Cliente} />
            <PrivateRoute path="/avaria" component={Avaria} />
            <PrivateRoute path="/complemento" component={Complemento} />
            <PrivateRoute path="/relatorio" component={Relatorio} />
            <PrivateRoute path="/guincho" component={Guincho} />
            <PrivateRoute path="/chamado/:id" component={Chamado} />
            <PrivateRoute path="/novo-chamado/:tipo" component={NovoChamado} />
            <PrivateRoute
              path="/options-chamados/:id"
              component={OptionChamados}
            />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
