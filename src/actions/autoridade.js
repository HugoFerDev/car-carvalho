import enviroment from "../enviroment";
import request from "../utils/request";

export const get_autoridades_request = () => (dispatch) => {
  request(`${enviroment.apiAmazon}/autoridades/listar`, 'PUT')
    .then(response => response.json())
    .then((data) => {
      dispatch({
        type: 'GET_AUTORIDADES_SUCCESS',
        response: data,
      });
    });
};

export const reset_autoridade = () => (dispatch) => {
  dispatch({ type: 'RESET_AUTORIDADE' });
}

export const create_chamado_request = chamado => (dispatch) => {
  return new Promise((res) => {
    dispatch({ type: 'CREATE_CHAMADO_REQUEST' });

    request(`${enviroment.apiAmazon}/chamados/cadastrar-chamado`, 'POST', chamado)
      .then(response => response.json())
      .then((data) => {
        localStorage.setItem('chamado', JSON.stringify(data));

        res(data)
        dispatch({ type: 'CREATE_CHAMADO_REQUEST_SUCCESS' });
      });
  })
};
