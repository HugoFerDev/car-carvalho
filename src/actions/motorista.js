import enviroment from "../enviroment"
import request from "../utils/request"

export const get_motoristas_request = () => (dispatch) => {
  request(`${enviroment.apiAmazon}/motoristas/listar`, 'PUT')
    .then(response => {
      return response.json()
    })
    .then((data) => {
      dispatch({
        type: 'GET_MOTORISTAS_SUCCESS',
        response: data
      })
    })
}
