import { FETCH_CHECKLIST, RESET_STATE, SEND_EMAIL } from './types';

import enviroment from '../enviroment';

export const saveCheckList = check => dispatch => new Promise((resolve, reject) => {
  Promise.all([
    fetch(`${enviroment.UrlAPI}/avaria`, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(check.avaria),
    }).then((response) => {
      if (response.status == 201) {
        return response.json();
      }
      throw response.json();
    }),
    fetch(`${enviroment.UrlAPI}/cliente`, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(check.cliente),
    }).then((response) => {
      if (response.status == 201) {
        return response.json();
      }
      throw response.json();
    }),
    fetch(`${enviroment.UrlAPI}/complemento`, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(check.complemento),
    }).then((response) => {
      if (response.status == 201) {
        return response.json();
      }
      throw response.json();
    }),
    fetch(`${enviroment.UrlAPI}/veiculo`, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(check.veiculo),
    }).then((response) => {
      if (response.status == 201) {
        return response.json();
      }
      throw response.json();
    }),
  ])
    .then(
      ([
        avaria,
        cliente,
        complemento,
        veiculo
      ]) => {
        const check = {
          avaria: avaria._id,
          cliente: cliente._id,
          complemento: complemento._id,
          veiculo: veiculo._id,
          placa: cliente.placa
        };

        fetch(`${enviroment.UrlAPI}/check-list`, {
          method: 'post',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(check),
        })
          .then((response) => {
            if (response.status == 201) {
              return response.json();
            }
            throw response.json();
          })
          .then((data) => {
            dispatch({ type: RESET_STATE });
            resolve(data);
          });
      },
    )
    .catch((err) => {
      console.log(err);
    });
});

export const fetchCheckList = (filtro) => (dispatch) => {
  fetch(`${enviroment.UrlAPI}/check-list?data=${filtro.data}&placa=${filtro.placa}`, {
    method: 'get',
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      }
      throw response.json();
    })
    .then((data) => {
      dispatch({ type: FETCH_CHECKLIST, response: data });
    });
};

export const sendEmail = (id, email) => dispatch => {

  fetch(`${enviroment.UrlAPI}/check-list/email`, {
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ id: id, email: email })
  })
    .then((response) => {
      if (response.status == 200) {
        return response
      }
      throw response.json();
    })
    .then((data) => {
      dispatch({ type: SEND_EMAIL, response: data });
    });
}