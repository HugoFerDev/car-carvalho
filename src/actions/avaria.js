import { ADD_AVARIA } from './types';

export function addAvaria(avaria) {
  return {
    type: ADD_AVARIA,
    avaria,
  };
}
