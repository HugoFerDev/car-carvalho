import { ADD_CLIENTE, BUSCAR_PLACA } from './types';

export function addCliente(cliente) {
  console.log('estou na action');
  return {
    type: ADD_CLIENTE,
    cliente,
  };
}

export const buscarPlaca = placa => dispatch => new Promise((resolve, reject) => {
  fetch(`https://www.contactbr.com.br/sinesp/busca?placa=${placa}`)
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      }
    })
    .then((data) => {
      dispatch({
        type: BUSCAR_PLACA,
        resposta: data,
      });
      resolve(data);
    });
});
