import request from "../utils/request";
import enviroment from "../enviroment";
import { FETCH_PATIOS_VEICULO, FETCH_TIPO_VEICULO } from "./types";

export const get_patios = () => async (dispatch) => {
  request(`${enviroment.apiAmazon}/patios/listar`, "PUT", { ativo: "1" })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      dispatch({
        type: FETCH_PATIOS_VEICULO,
        response: data,
      });
    });
};

export const get_tipo_veiculo = empresaId => async dispatch => {
  request(`${enviroment.apiAmazon}/tipos_veiculo/listar?idEmpresa=1`)
    .then(response => {
      return response.json();
    })
    .then(data => {
      dispatch({
        type: FETCH_TIPO_VEICULO,
        response: data
      });
    });
};
