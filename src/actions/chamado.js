import enviroment from "../enviroment";
import request from "../utils/request";
import { FETCH_TIPO_VEICULO } from "./types";

export const get_chamado_request = id => async dispatch => {
  request(`${enviroment.apiAmazon}/chamados/pega-localizacao/${id}`)
    .then(response => response.json())
    .then(data => {
      (async () => {
        for (let i = 0; i < data.length; i++) {
          data[i].endereco = await get_address_request(
            data[i].latitude_atendimento,
            data[i].longitude_atendimento
          );
        }
        dispatch({
          type: "GET_CHAMADO_SUCCESS",
          response: data
        });
      })();
    });
};

export const accept_chamado_request = chamado => dispatch => {
  request(`${enviroment.apiAmazon}/chamados/aceite-chamado`, "POST", chamado)
    .then(response => response.json())
    .then(data => {});
};

export const update_location_chamado_request = chamado => dispatch => {
  request(`${enviroment.apiAmazon}/chamados/envia-localizacao`, "POST", chamado)
    .then(response => response.json())
    .then(data => {});
};

export const get_chamados_by_motorista_request = id => async dispatch => {
  request(`${enviroment.apiAmazon}/chamados/busca-novochamado/${id}`)
    .then(response => response.json())
    .then(data => {
      (async () => {
        for (let i = 0; i < data.length; i++) {
          data[i].endereco = await get_address_request(
            data[i].latitude_chamador,
            data[i].longitude_chamador
          );
        }
        dispatch({
          type: "GET_CHAMADO_SUCCESS",
          response: data
        });
      })();
    });
};

export const get_chamados_by_autoridade_request = id => async dispatch => {
  request(
    `${enviroment.apiAmazon}/chamados/busca-chamados-autoridade?idautoridade=${id}`
  )
    .then(response => response.json())
    .then(data => {
      const chamados = data.chamados;
      const tipoVeiculos = data.tipos_veiculo;

      (async () => {
        for (let i = 0; i < chamados.length; i++) {
          chamados[i].endereco = await get_address_request(
            chamados[i].latitude_atendimento,
            chamados[i].longitude_atendimento
          );
        }

        dispatch({
          type: "GET_CHAMADO_SUCCESS",
          response: chamados
        });

        // dispatch({
        //   type: FETCH_TIPO_VEICULO,
        //   response: tipoVeiculos
        // });
      })();
    });
};

const get_address_request = async (lat, lon) => {
  const response = await request(
    `https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lon}&format=json`
  );
  const data = await response.json();

  return data;
};
