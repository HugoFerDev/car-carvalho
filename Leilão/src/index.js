import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';
import 'leaflet/dist/leaflet.css'

import * as serviceWorker from './serviceWorker';

import App from './App';
import L from 'leaflet';
import React from 'react';
import ReactDOM from 'react-dom';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

let DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;
serviceWorker.register();
ReactDOM.render(<App />, document.getElementById('root'));
