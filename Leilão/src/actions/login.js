import enviroment from "../enviroment";

export const login_request = (login, senha, deviceToken) => (dispatch) => {
  console.log({
    login,
    senha,
    deviceToken,
    tiposistema: 'app',
    empresa: 1
  })
  return new Promise((res, rej) => {
    fetch(`${enviroment.apiAmazon}/autenticacao/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        login,
        senha,
        deviceToken,
        tiposistema: 'app',
        empresa: 1
      })
    })
      .then((response) => {
        if (!response.ok) throw response.json()
        return response.json();
      })
      .then((data) => {
        res(data);
        dispatch({
          type: 'LOGIN_SUCCESS',
          response: data
        })
      })
      .catch(() => {
        rej('error');
      })
  })
}
