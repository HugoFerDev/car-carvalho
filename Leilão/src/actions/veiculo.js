import { ADD_VEICULO } from './types';

export function addVeiculo(veiculo) {
  return {
    type: ADD_VEICULO,
    veiculo,
  };
}
