import {ADD_CLIENTE, BUSCAR_PLACA} from './types';

export function addCliente(cliente) {
  return {
    type: ADD_CLIENTE,
    cliente,
  };
}
