import { ADD_CONFERENTE } from './types';

export function addConferente(conferente) {
  return {
    type: ADD_CONFERENTE,
    conferente,
  };
}
