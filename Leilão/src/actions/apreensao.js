import { ADD_APREENSAO } from './types';

export function addApreensao(apreensao) {
  return {
    type: ADD_APREENSAO,
    apreensao,
  };
}
