import { ADD_AVARIA, ADD_AVARIA_CHECK } from './types';

export function addAvaria(avaria) {
  return {
    type: ADD_AVARIA,
    avaria,
  };
}
export function addAvariaCheck(avaria) {
  return {
    type: ADD_AVARIA_CHECK,
    response: avaria,
  };
}
