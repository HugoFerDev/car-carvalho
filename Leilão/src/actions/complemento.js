import { ADD_COMPLEMENTO } from './types';

export function addComplemento(complemento) {
  return {
    type: ADD_COMPLEMENTO,
    complemento,
  };
}
