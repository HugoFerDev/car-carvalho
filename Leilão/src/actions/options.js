import request from '../utils/request';
import enviroment from '../enviroment';

export const get_tipo_veiculo = empresaId => async dispatch => {
  request(`${enviroment.apiAmazon}/tipos_veiculo/listar?idEmpresa=${empresaId}`)
    .then(response => {
      return response.json();
    })
    .then(data => {
      dispatch({
        type: 'FETCH_TIPO_VEICULO_SUCCESS',
        response: data,
      });
    });
};
