import { height, lineHeight, width } from 'styled-system';

import styled from 'styled-components';

export const StyledTableFooter = styled.div`
  width: 100%;
  padding: 1em;
  display: flex;
  justify-content: center;

  > button {
    width: 30%
  }
`

export const TableContainer = styled.div`
  border: none;
  @media (min-width: 768px) {
    border: 1px solid #c3c3c3;
    overflow:auto;
  }
`

export const Table = styled.div`
  display: block;
  &:last-child {
    border-bottom: none;
  }
  @media (min-width: 768px) {
    width: 100%;
    display: table;
    color: inherit;
  }
`

export const TableHeader = styled.div`
  display: block;
  @media (min-width: 768px) {
    font-weight: 600;
    color: inherit;
    background-color: hsla(201, 100%, 96%, 1);
  }
`

export const TableRow = styled.div`
  display: block;
  border-bottom: 1px solid #c3c3c3;
  padding-top: 16px;
  @media (min-width: 768px) {
    padding-top: 0;
    display: table-row;
    border-bottom: none;
    box-shadow: inset 0 -1px 0 0 #c3c3c3;
    &:hover {
      background-color: hsla(201,  20%, 96%, 1);
    }
  }
`

export const TableHeaderRow = styled(TableRow)`
  border-bottom: none;
  @media (min-width: 768px) {
    font-weight: 500;
    color: inherit;
    background-color: hsla(201,  20%, 96%, 1);
  }
`

export const TableCell = styled.div`
  display: flex;
  margin-bottom: 10px;
  line-height: 20px;
  justify-content: ${props => props.justify ? props.justify : 'flex-start'};
  margin-left: 10px;
  min-width: 5em;
  word-wrap: break-word;
  word-break: break-all;
  text-align: ${props => props.align ? props.align : 'center'};
  @media (max-width: 767px) {
    &:before {
      content: attr(data-title);
      min-width: 37%;
      font-size: 10px;
      font-weight: bold;
      text-transform: uppercase;
      color: #c3c3c3;
      display: flex;
    }
  }
  @media (min-width: 768px) {
    display: table-cell;
    vertical-align: middle;
    font-size: inherit;
    padding-right: 16px;
    min-width: 5em;
    justify-content: ${props => props.justify ? props.justify : 'flex-start'};
    max-width: ${props => props.maxWidth};
    &:first-child {
      height: 40px;
      padding-left: 16px;
    }
  }
  ${width}
  ${height}
  ${lineHeight}
`

export const TableHeaderCell = styled(TableCell)`
  margin-bottom: 0;
  cursor: pointer;
  text-align: ${props => props.align ? props.align : 'center'};
  @media (max-width: 767px) {
    display: none;
  }

  > .order-hidden {
    visibility: hidden
  }

  :hover > .order-hidden {
    visibility: visible
  }
`
