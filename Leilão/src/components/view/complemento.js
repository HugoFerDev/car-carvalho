import { compose } from 'redux';
import { withFormik } from 'formik';
import { connect } from 'react-redux';
import React, { Component, useState } from 'react';
import Slider from 'react-input-slider';

import {
  Container, Row, Col, Card, Form, Button, Image,
} from 'react-bootstrap';
import Page from './page';
import { addComplemento } from '../../actions/complemento';
import Util from '../../utils/util';

class Complemento extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {};
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue } = this.props;

    const file = event.target.files[0];
    setFieldValue(`${item}`, await Util.readUploadedFileAsText(file));
  }

  changeValue = (e) => {
    const { setFieldValue } = this.props;
    setFieldValue('combustivel', e.x);
  };

  handleCheck(item, event) {
    const { setFieldValue } = this.props;
    setFieldValue(item, event.target.value);
  }

  render() {
    const { values, handleChange, handleSubmit } = this.props;

    return (
      <Page title="COMPLEMENTO" pageBack="veiculo">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card>
                    <Card.Body className="shadow rounded">

                      <Form.Group>
                        <Form.Label>Marca da Bateria:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.marcaBateria}
                          onChange={handleChange}
                          name="marcaBateria"
                        />
                      </Form.Group>

                      <div className="d-flex justify-content-around flex-wrap">
                        <div className="ml-2">
                          <div className="d-flex justify-content-between flex-column flex-wrap">
                            <Form.Label className="w-100">Pintura:</Form.Label>
                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputPinturaBom"
                                  className="round"
                                  value="bom"
                                  checked={values.pintura == 'bom'}
                                  name="pintura"
                                  onChange={event => this.handleCheck('pintura', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputPinturaBom" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputPinturaBom">Bom</Form.Label>
                            </div>

                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputPinturaRegular"
                                  className="round"
                                  value="regular"
                                  checked={values.pintura == 'regular'}
                                  name="pintura"
                                  onChange={event => this.handleCheck('pintura', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputPinturaRegular" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputPinturaRegular">Regular</Form.Label>
                            </div>

                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputPinturaRuim"
                                  className="round"
                                  value="ruim"
                                  checked={values.pintura == 'ruim'}
                                  name="pintura"
                                  onChange={event => this.handleCheck('pintura', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputPinturaRuim" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputPinturaRuim">Ruim</Form.Label>
                            </div>
                          </div>
                        </div>

                        <div className="ml-2 mt-2">
                          <div className="d-flex justify-content-between flex-column flex-wrap">
                            <Form.Label className="w-100">Tapecaria:</Form.Label>
                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputTapecariaBom"
                                  className="round"
                                  value="bom"
                                  checked={values.tapecaria == 'bom'}
                                  name="tapecaria"
                                  onChange={event => this.handleCheck('tapecaria', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputTapecariaBom" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputTapecariaBom">Bom</Form.Label>
                            </div>

                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputTapecariaRegular"
                                  className="round"
                                  value="regular"
                                  checked={values.tapecaria == 'regular'}
                                  name="tapecaria"
                                  onChange={event => this.handleCheck('tapecaria', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputTapecariaRegular" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputTapecariaRegular">Regular</Form.Label>
                            </div>

                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputTapecariaRuim"
                                  className="round"
                                  value="ruim"
                                  checked={values.tapecaria == 'ruim'}
                                  name="tapecaria"
                                  onChange={event => this.handleCheck('tapecaria', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputTapecariaRuim" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputTapecariaRuim">Ruim</Form.Label>
                            </div>
                          </div>
                        </div>

                        <div className="ml-2 mt-2">
                          <div className="d-flex justify-content-between flex-column flex-wrap">
                            <Form.Label className="w-100">Pneus:</Form.Label>
                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputPneusBom"
                                  className="round"
                                  value="bom"
                                  checked={values.pneus == 'bom'}
                                  name="pneus"
                                  onChange={event => this.handleCheck('pneus', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputPneusBom" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputPneusBom">Bom</Form.Label>
                            </div>

                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputPneusRegular"
                                  className="round"
                                  value="regular"
                                  checked={values.pneus == 'regular'}
                                  name="pneus"
                                  onChange={event => this.handleCheck('pneus', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputPneusRegular" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputPneusRegular">Regular</Form.Label>
                            </div>

                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  id="inputPneusRuim"
                                  className="round"
                                  value="ruim"
                                  checked={values.pneus == 'ruim'}
                                  name="pneus"
                                  onChange={event => this.handleCheck('pneus', event)}
                                />
                                <Form.Label className="cursor-pointer" htmlFor="inputPneusRuim" />
                              </div>
                              <Form.Label className="cursor-pointer" htmlFor="inputPneusRuim">Ruim</Form.Label>
                            </div>
                          </div>
                        </div>

                      </div>

                      <div className='d-flex align-items-start mt-4'>
                        <div className="d-flex align-items-center flex-column">
                          <div className="p-0">
                            {values.combustivel ? (
                              <Image
                                className="img-thumbnail w-rem-6"
                                src={values.combustivel}
                              />
                            ) : (
                                <i className="fa fa-picture-o fa-3x" aria-hidden="true" />
                              )}
                          </div>

                          <Button
                            htmlFor="inputCombustivel"
                            variant="link"
                            onClick={() => document.getElementById('inputCombustivel').click()}
                          >
                            <input
                              id="inputCombustivel"
                              style={{ display: 'none' }}
                              type="file"
                              accept="image/*"
                              capture
                              onChange={event => this.setImagem('combustivel', event)}
                            />
                            Combustível
                            </Button>
                        </div>
                      </div>

                      <Button onClick={handleSubmit} className="w-100 mt-4" variant="primary">
                        Proximo
                      </Button>
                    </Card.Body>
                  </Card>

                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { complemento: state.complemento };
}

export default compose(
  connect(
    mapStateToProps,
    { addComplemento },
  ),
  withFormik({
    mapPropsToValues: props => props.complemento,

    handleSubmit: (values, { props }) => {
      props.addComplemento(values);
      props.history.push('/avaria');
    },
  }),
)(Complemento);
