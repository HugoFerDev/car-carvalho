import {
  Button,
  Card,
  Carousel,
  Col,
  Container,
  Form,
  Row,
  Spinner,
} from "react-bootstrap";
import React, { Component } from "react";

import Page from "./page";
import Util from "../../utils/util";
import { addConferente } from "../../actions/conferente";
import { saveChecklistRequest } from "../../actions/check-list";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFormik } from "formik";

class Conferente extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {};

    this.getLocation = this.getLocation.bind(this);
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue, values } = this.props;
    const { files } = event.target;
    const imagens = [];
    for (let index = 0; index < files.length; index++) {
      imagens.push(Util.readUploadedFileAsText(files[index]));
    }

    Promise.all(imagens).then((data) => {
      const res = values[item];
      if (res.length >= 10) return;
      data.forEach((element) => {
        res.push(element);
      });

      setFieldValue(`${item}`, res);
    });
  }

  getLocation () {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        this.props.setFieldValue('latitude', latitude);
        this.props.setFieldValue('longitude', longitude);
        alert('Localização Registrada');
      },
      () => { },
      {
        timeout: 3000,
        enableHighAccuracy: true,
      }
    )
  }

  render() {
    const { values, handleSubmit, handleChange, conferente } = this.props;

    const {
      imagemLateralDireita,
      imagemLateralEsquerda,
      imagemTraseira,
      imagemDianteira,
      imagemTeto,
      imagemMotor,
      imagemChassi,
      imagemOutros,
      imagemPainel,
      imagemCofreMotor,
      imagemPlaca,
      imagemLote,
      imagemChassiPinado,
      imagemPlacaCortada,
    } = values;

    if(conferente.loading) {
      return (
        <div
          className="d-flex align-items-center justify-content-center"
          style={{ height: '100vh' }}
        >
          <div className="p-4 rounded" style={{ backgroundColor: '#fff' }}>
            <Spinner animation="border" variant="primary" />
          </div>
        </div>
      );
    }

    return (
      <Page title="DADOS CONFERENTE" pageBack="veiculo">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card>
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Lateral Direita{" "}
                          {`(${imagemLateralDireita.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document
                              .getElementById("imagemLateralDireita")
                              .click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemLateralDireita"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemLateralDireita"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemLateralDireita", event)
                        }
                      />

                      <Carousel>
                        {values.imagemLateralDireita.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Lateral Direita</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Lateral Esquerda{" "}
                          {`(${imagemLateralEsquerda.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document
                              .getElementById("imagemLateralEsquerda")
                              .click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemLateralEsquerda"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemLateralEsquerda"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemLateralEsquerda", event)
                        }
                      />

                      <Carousel>
                        {values.imagemLateralEsquerda.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Lateral Esquerda</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Traseira {`(${imagemTraseira.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemTraseira").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemTraseira"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemTraseira"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemTraseira", event)
                        }
                      />

                      <Carousel>
                        {values.imagemTraseira.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Traseira</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Dianteira {`(${imagemDianteira.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemDianteira").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemDianteira"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemDianteira"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemDianteira", event)
                        }
                      />

                      <Carousel>
                        {values.imagemDianteira.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Dianteira</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Teto {`(${imagemTeto.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemTeto").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemTeto"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemTeto"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemTeto", event)
                        }
                      />

                      <Carousel>
                        {values.imagemTeto.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Teto</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Numero Motor {`(${imagemMotor.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemMotor").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemMotor"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemMotor"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemMotor", event)
                        }
                      />

                      <Carousel>
                        {values.imagemMotor.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Numero Motor</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Numero Chassi {`(${imagemChassi.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemChassi").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemChassi"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemChassi"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemChassi", event)
                        }
                      />

                      <Carousel>
                        {values.imagemChassi.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Numero Chassi</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Painel {`(${imagemPainel.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemPainel").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemPainel"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemPainel"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemPainel", event)
                        }
                      />

                      <Carousel>
                        {values.imagemPainel.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Painel</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Cofre Motor {`(${imagemCofreMotor.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemCofreMotor").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemCofreMotor"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemCofreMotor"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemCofreMotor", event)
                        }
                      />

                      <Carousel>
                        {values.imagemCofreMotor.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Cofre Motor</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>


                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Placa {`(${imagemPlaca.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemPlaca").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemPlaca"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemPlaca"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemPlaca", event)
                        }
                      />

                      <Carousel>
                        {values.imagemPlaca.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Placa</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Lote {`(${imagemLote.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemLote").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemLote"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemLote"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemLote", event)
                        }
                      />

                      <Carousel>
                        {values.imagemLote.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Lote</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Chassi Pinado {`(${imagemChassiPinado.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemChassiPinado").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemChassiPinado"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemChassiPinado"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemChassiPinado", event)
                        }
                      />

                      <Carousel>
                        {values.imagemChassiPinado.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Chassi Pinado</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>

                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Placa Cortada {`(${imagemPlacaCortada.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemPlacaCortada").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemPlacaCortada"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemPlacaCortada"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemPlacaCortada", event)
                        }
                      />

                      <Carousel>
                        {values.imagemPlacaCortada.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Placa Cortada</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>
                    </Card.Body>
                  </Card>




                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between">
                        <Card.Title>
                          {" "}
                          Outros {`(${imagemOutros.length}/10)`}
                        </Card.Title>
                        <i
                          style={{ fontSize: "25px" }}
                          onClick={() =>
                            document.getElementById("imagemOutros").click()
                          }
                          className="fa fa-camera cursor-pointer"
                        />
                      </div>

                      <input
                        id="imagemOutros"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemOutros"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemOutros", event)
                        }
                      />

                      <Carousel>
                        {values.imagemOutros.map((img) => (
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={img}
                              alt="First slide"
                            />
                            <Carousel.Caption>
                              <h3>Outros</h3>
                            </Carousel.Caption>
                          </Carousel.Item>
                        ))}
                      </Carousel>

                      <input
                        id="imagemMotor"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemMotor"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemMotor", event)
                        }
                      />

                      <Form.Group>
                        <Form.Label>RFID/IDENTIFICADOR:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.rfidIdentificador}
                          onChange={handleChange}
                          name="rfidIdentificador"
                        />
                      </Form.Group>


                      <Button
                        onClick={this.getLocation}
                        className="w-100 mt-4"
                        variant="primary"
                      >
                        CAPTURAR LOCALIZAÇÃO
                      </Button>

                      <Button
                        onClick={handleSubmit}
                        className="w-100 mt-4"
                        variant="primary"
                      >
                        Gravar Dados
                      </Button>
                    </Card.Body>
                  </Card>

                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { conferente: state.conferente, check: state };
}

export default compose(
  connect(mapStateToProps, { addConferente, saveChecklistRequest }),
  withFormik({
    mapPropsToValues: (props) => props.conferente,

    handleSubmit: (values, { props }) => {
      props.addConferente(values);
      let check = props.check;
      check = Object.assign(
        {
          ...check,
          cliente: {
            ...check.cliente,
            chamadoId: localStorage.getItem("chamado_id"),
          },
        },
        { conferente: values },
        {acesso: localStorage.getItem('acesso')}
      );
      props.saveChecklistRequest(check)
      .then(() => {
        props.history.push("/menu");
      })
    },
  })
)(Conferente);
