import "leaflet-offline";

import * as chamado_action from "../../actions/chamado";

import { Button, Modal } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import {
  Table,
  TableCell,
  TableContainer,
  TableHeaderCell,
  TableHeaderRow,
  TableRow
} from "../styles/Table";

import Page from "./page";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import enviroment from "../../enviroment";
import history from "../../utils/history";
import moment from "moment";

const OptionChamado = ({
  match,
  chamados,
  // get_chamados_by_motorista_request,
  accept_chamado_request
}) => {
  const [modal, setModal] = useState({ open: false, item: {} });

  useEffect(() => {
    // get_chamados_by_motorista_request(match.params.id);
    const cAtual = chamados.filter(chamado => chamado.status)[0];
    localStorage.removeItem("chamado_atual");
    if (cAtual) {
      localStorage.setItem(
        "chamado_atual",
        JSON.stringify({ ...cAtual, chamado: cAtual })
      );
    }
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      // get_chamados_by_motorista_request(match.params.id);
    }, enviroment.TIME_REFRESH);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    const cAtual = chamados.filter(chamado => chamado.status)[0];
    localStorage.removeItem("chamado_atual");
    if (cAtual) {
      localStorage.setItem(
        "chamado_atual",
        JSON.stringify({ ...cAtual, chamado: cAtual })
      );
    }
  }, [chamados]);

  const handleClose = () => {
    setModal({ ...modal, open: false });
  };

  const handleShow = item => {
    setModal({ ...modal, open: true, item: item });
  };

  const checkChamado = id => {
    localStorage.setItem("chamado_id", id);
    history.push("/cliente");
  };

  const acceptChamado = () => {
    navigator.geolocation.getCurrentPosition(position => {
      let lat = position.coords.latitude;
      let lng = position.coords.longitude;

      const chamado_atual = {
        idchamado: modal.item.id,
        idmotorista: match.params.id,
        latitude: lat,
        longitude: lng
      };
      accept_chamado_request(chamado_atual);

      localStorage.setItem(
        "chamado_atual",
        JSON.stringify({ ...chamado_atual, chamado: modal.item })
      );
      history.push("/menu");
    });
  };

  return (
    <Page title="CHAMADO" pageBack="menu">
      <div className="d-flex mt-3 flex-column justify-content-between h-80vh">
        <TableContainer>
          <Table>
            <TableHeaderRow>
              <TableHeaderCell>#</TableHeaderCell>
              <TableHeaderCell>Chamador</TableHeaderCell>
              <TableHeaderCell>Autoridade</TableHeaderCell>
              <TableHeaderCell>Data</TableHeaderCell>
              <TableHeaderCell>Endereço</TableHeaderCell>
              <TableHeaderCell>Status</TableHeaderCell>
              <TableHeaderCell>Equipamento</TableHeaderCell>
              <TableHeaderCell>Estado Veículo</TableHeaderCell>
              <TableHeaderCell>Tipo Veículo</TableHeaderCell>
              <TableHeaderCell justify="space-between">Ações</TableHeaderCell>
            </TableHeaderRow>
            {chamados.map((chamado, index) => (
              <TableRow key={chamado.id}>
                <TableCell data-title="#">{index + 1}</TableCell>
                <TableCell data-title="Chamador">
                  {chamado.nome_chamador}
                </TableCell>
                <TableCell data-title="Autoridade">
                  {chamado.autoridade}
                </TableCell>
                <TableCell data-title="Data">
                  {moment(chamado.data_hora_chamador).format(
                    "DD/MM/YYYY HH:mm"
                  )}
                </TableCell>
                <TableCell align="justify" data-title="Endereço">
                  {chamado.endereco &&
                    chamado.endereco.address &&
                    `${chamado.endereco.address.road}, ${chamado.endereco.address.city_district}, ${chamado.endereco.address.state}`}
                </TableCell>
                <TableCell data-title="Status">
                  {chamado.status ? "Atendendo" : "Aguardando"}
                </TableCell>
                <TableCell
                  data-title="Equipamento"
                  className="transform-uppercase"
                >
                  {chamado.equipamento}
                </TableCell>
                <TableCell
                  data-title="Estado Veículo"
                  className="transform-uppercase"
                >
                  {chamado.estado_veiculo}
                </TableCell>
                <TableCell
                  data-title="Tipo Veículo"
                  className="transform-uppercase"
                >
                  {chamado.tipo_veiculo}
                </TableCell>

                <TableCell data-title="Ações">
                  <div className="d-flex justify-content-between min-w-5-em">
                    <i
                      className="fa fa-search cursor-pointer"
                      onClick={() => handleShow(chamado)}
                    />

                    <i
                      className="fa fa-check-square-o cursor-pointer"
                      onClick={() => checkChamado(chamado.id)}
                    />
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </Table>
        </TableContainer>

        <Modal show={modal.open} onHide={handleClose} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Detalhes do Chamado</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="d-flex">
              <div>Chamador: </div>
              <span>{modal.item.nome_chamador}</span>
            </div>

            <div className="d-flex">
              <div>Autoridade: </div>
              <span>{modal.item.autoridade}</span>
            </div>

            <div className="d-flex">
              <div>Data/Hora: </div>
              <span>{modal.item.data_hora_chamador}</span>
            </div>

            <div className="d-flex">
              <div>Localização: </div>
              <span>
                {modal.item.endereco &&
                  modal.item.endereco.address &&
                  `${modal.item.endereco.address.road}, ${modal.item.endereco.address.city_district}, ${modal.item.endereco.address.state}`}
              </span>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Fechar
            </Button>

            <Button
              variant="primary"
              onClick={acceptChamado}
              disabled={modal.item.status}
            >
              Aceitar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </Page>
  );
};

function mapStateToProps({ chamado }) {
  return {
    chamados: chamado.chamados
  };
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...chamado_action }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OptionChamado);
