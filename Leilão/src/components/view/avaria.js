import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
  Carousel,
} from "react-bootstrap";
import React, { Component } from "react";
import { addAvaria } from "../../actions/avaria";

import Page from "./page";
import Util from "../../utils/util";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFormik } from "formik";

class Avaria extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    this.setImagem = this.setImagem.bind(this);
  }

  async componentDidMount() {
    const { setFieldValue } = this.props;
    setFieldValue("os", await Util.generateOS());
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue, values } = this.props;

    if (item == "veiculo") {
      const { files } = event.target;
      const imagens = values.imagens.veiculo;
      for (let index = 0; index < files.length; index++) {
        imagens.push(Util.readUploadedFileAsText(files[index]));
      }

      Promise.all(imagens).then((values) => {
        setFieldValue(`imagens.${item}`, values);
      });
    } else {
      const file = event.target.files[0];
      setFieldValue(`imagens.${item}`, await Util.readUploadedFileAsText(file));
    }
  }

  render() {
    const { values, handleSubmit } = this.props;

    return (
      <Page title="FOTOS AVARIAS" pageBack="conferente">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Carousel>
                    {
                      (values.avarias || []).map(img => <Carousel.Item>
                        <img
                          className="d-block w-100"
                          src={img.imagens}
                          alt={img.tipo}
                        />
                        <Carousel.Caption>
                          <h3>{img.tipo}</h3>
                        </Carousel.Caption>
                      </Carousel.Item>)
                    }
                  </Carousel>


                  <Button
                    className="w-100 mt-4"
                    variant="primary"
                    onClick={handleSubmit}
                  >
                    Proximo
                  </Button>
                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { avaria: state.avaria };
}

export default compose(
  connect(mapStateToProps, { addAvaria }),
  withFormik({
    mapPropsToValues: (props) => props.avaria,

    handleSubmit: (values, { props }) => {
      props.addAvaria(values);
      props.history.push("/conferente");
    },
  })
)(Avaria);
