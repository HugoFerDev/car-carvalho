import { Redirect, Route, Router, Switch } from "react-router-dom";

import Conferente from './components/view/conferente';
import Chamado from "./components/view/chamado";
import Cliente from './components/view/cliente';
import Complemento from './components/view/complemento';
import Menu from './components/view/menu';
import OptionChamados from "./components/view/option-chamados";
import NovoChamado from "./components/view/novo-chamado";
import { Provider } from 'react-redux';
import React from 'react';
import Relatorio from './components/view/relatorio';
import Veiculo from './components/view/veiculo';
import configureStore from './store';
import history from './utils/history';
import Login from "./components/view/login";
import Apreensao from "./components/view/apreensao";
import Avaria from "./components/view/avaria";

const store = configureStore();

const PrivateRoute = ({ ...props }) =>
  localStorage.getItem('acesso')
    ? <Route {...props} />
    : <Redirect to="/" />

const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Login} />

        <PrivateRoute path="/menu" component={Menu} />
        <PrivateRoute path="/veiculo" component={Veiculo} />
        <PrivateRoute path="/avaria" component={Avaria} />
        <PrivateRoute path="/cliente" component={Cliente} />
        <PrivateRoute path="/conferente" component={Conferente} />
        <PrivateRoute path="/complemento" component={Complemento} />
        <PrivateRoute path="/relatorio" component={Relatorio} />
        <PrivateRoute path="/chamado/:id" component={Chamado} />
        <PrivateRoute path="/novo-chamado/:tipo" component={NovoChamado} />
        <PrivateRoute path="/apreensao" component={Apreensao} />
      </Switch>
    </Router>

  </Provider>
);

export default App;
