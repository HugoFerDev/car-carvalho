import {ADD_CONFERENTE, CHECKLIST_SAVED_FAILED, CHECKLIST_SAVED_SUCCESS, RESET_STATE} from '../actions/types';

const initialState = {
  descricaoLateralDireita: '',
  imagemLateralDireita: [],

  descricaoLateralEsquerda: '',
  imagemLateralEsquerda: [],

  descricaoTraseira: '',
  imagemTraseira: [],

  descricaoDianteira: '',
  imagemDianteira: [],

  descricaoTeto: '',
  imagemTeto: [],

  descricaoOutros: '',
  imagemOutros: [],
  documentos: [],

  imagemChassi: [],
  imagemMotor: [],
  imagemPainel: [],
  imagemCofreMotor: [],
  imagemPlaca: [],
  imagemLote: [],
  imagemChassiPinado: [],
  imagemPlacaCortada: [],

  observacao: '',
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CONFERENTE: {
      const {conferente} = action;
      return Object.assign({}, state, {...conferente, loading: true});
    }

    case CHECKLIST_SAVED_FAILED: {
      return {...state, loading: false}
    }

    case RESET_STATE:
    case CHECKLIST_SAVED_SUCCESS: {
      return {
        descricaoLateralDireita: '',
        imagemLateralDireita: [],

        descricaoLateralEsquerda: '',
        imagemLateralEsquerda: [],

        descricaoTraseira: '',
        imagemTraseira: [],

        descricaoDianteira: '',
        imagemDianteira: [],

        descricaoTeto: '',
        imagemTeto: [],

        descricaoOutros: '',
        imagemOutros: [],
        documentos: [],

        imagemChassi: [],
        imagemMotor: [],
        imagemPainel: [],
        imagemCofreMotor: [],
        imagemPlaca: [],
        imagemLote: [],
        imagemChassiPinado: [],
        imagemPlacaCortada: [],

        observacao: '',
        loading: false,
      };
    }

    default: {
      return state;
    }
  }
};
