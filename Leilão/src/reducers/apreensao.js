import {ADD_APREENSAO, RESET_STATE} from './../actions/types';

const initialState = {
  chaves: false,
  blitz: false,
  guinchoColetivo: false,
  semDocumentoCrv: false,
  rouboFurto: false,
  traficoDrogas: false,
  policialCivil: false,
  pedirBaixa: false,
  motorComQueixa: false,
  leasing: false,
  judicial: false,
  foraCirculacao: false,
  emTela: false,
  crimesTransito: false,
  adulterado: false,
  kmPercorrido: '',
  ordemServico: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_APREENSAO: {
      const {apreensao} = action;
      return Object.assign({}, state, apreensao);
    }

    case RESET_STATE: {
      return initialState;
    }

    default: {
      return state;
    }
  }
};
