import autoridade from './autoridades';
import avaria from './avaria';
import chamado from './chamado';
import checklist from './checklist';
import cliente from './cliente';
import {combineReducers} from 'redux';
import complemento from './complemento';
import motorista from './motorista';
import patio from './patio';
import veiculo from './veiculo';
import apreensao from './apreensao';
import conferente from './conferente';
import options from './options';

export default combineReducers({
  apreensao,
  veiculo,
  options,
  cliente,
  conferente,
  avaria,
  complemento,
  checklist,
  autoridade,
  motorista,
  chamado,
  patio,
});
