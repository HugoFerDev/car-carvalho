const initialState = {
  tipoVeiculo: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_TIPO_VEICULO_SUCCESS': {
      return {
        ...state,
        tipoVeiculo: action.response,
      };
    }

    default: {
      return state;
    }
  }
};
