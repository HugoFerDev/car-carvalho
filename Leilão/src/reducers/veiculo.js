import {ADD_VEICULO, RESET_STATE} from '../actions/types';

const initialState = {
  dvd: false,
  tela: false,
  frente: false,
  tapete: false,
  manual: false,
  macaco: false,
  antena: false,
  calotas: false,
  radioCd: false,
  extintor: false,
  pneuStep: false,
  documento: false,
  triangulo: false,
  chaveRoda: false,
  rodaComum: false,
  acendedor: false,
  chaveReserva: false,
  rodaEspecial: false,
  carroFuncionando: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_VEICULO: {
      const {veiculo} = action;
      return Object.assign({}, state, veiculo);
    }

    case RESET_STATE: {
      return initialState;
    }

    default: {
      return state;
    }
  }
};
