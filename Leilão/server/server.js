let express = require('express');

let app = express();
let sinesp = require('sinesp-nodejs');

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.get('/busca', (req, res) => {
  sinesp.consultaPlaca(req.query.placa).then((dados) => {
    res.json(dados);
  }).catch((err) => { }
  );
});

let port = process.env.PORT || 8090;

app.listen(port, () => {
  console.log('Example app listening on port 8090!');
});
