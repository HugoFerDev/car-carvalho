chrome.runtime.onInstalled.addListener(() => {
  chrome.alarms.create('refresh', { periodInMinutes: 0.1 });
});

chrome.alarms.onAlarm.addListener((alarm) => {
  updateLocation();
});

function updateLocation() {
  const chamadoAtual = JSON.parse(window.localStorage.getItem('chamado_atual'));

  if (chamadoAtual) {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {

        fetch(`https://api-carvalho-patios.herokuapp.com/chamados/envia-localizacao`, {
          method: 'POST',
          body: JSON.stringify({ idchamado: chamadoAtual.id, latitude: latitude, longitude: longitude })
        })
      },
      () => { },
      {
        timeout: 3000,
        enableHighAccuracy: true,
      }
    )
  }
}
