let cacheNameStatic = 'car-v2';

let currentCacheNames = [cacheNameStatic];

let cachedUrls = [
  './../src',
  './../src/App.css',
  './../src/App.js',
  './../src/App.test.js',
  './../src/index.css',
  './../src/index.js',
  './../src/components',
];

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(cacheNameStatic).then(function (cache) {
      return cache.addAll(cachedUrls);
    })
  );
});

self.addEventListener('activate', (event) => {
  event.waitUntil(
    caches.keys()
      .then(function (cacheNames) {
        return Promise.all(
          cacheNames.map(function (cacheName) {
            if (currentCacheNames.indexOf(cacheName) === -1) {
              return caches.delete(cacheName);
            }
          })
        );
      })
  );
});

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.match(event.request).then(function (resp) {
      return resp || fetch(event.request).then(function (response) {
        return caches.open(cacheNameStatic).then(function (cache) {
          console.log(event)
          cache.put(event.request, response.clone());
          return response;
        });
      });
    })
  );
});
