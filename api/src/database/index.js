const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost/atlantica");
// mongoose.connect('mongodb://atlantica:atlantica2019@ds345587.mlab.com:45587/checklist_atlantica');
mongoose.Promise = global.Promise;

module.exports = mongoose;
