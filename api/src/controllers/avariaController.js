const express = require('express');

const Avaria = require('../models/avaria');

const router = express.Router();

router.post('/', (req, res) => {
  try {
    return Avaria.create(req.body).then(data => res.status(201).send(data));
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    });
  }
});

router.get('/', async (req, res) => {
  try {
    const avaria = await Avaria.find({});

    return res.status(200).send(avaria);
  } catch (err) {
    return res.status(404).send({ error: 'Não possui registros cadastrados.' });
  }
});

module.exports = app => app.use('/avaria', router);
