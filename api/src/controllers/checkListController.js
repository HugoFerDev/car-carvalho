const express = require('express');

const CheckList = require('../models/checkList');
const Cliente = require('../models/cliente');

const router = express.Router();
const toDate = require('../utils/toDate');

const nodemailer = require('nodemailer');
const checkTemplate = require('./../templates/check');
var inlineBase64 = require('nodemailer-plugin-inline-base64');

router.post('/', (req, res) => {
  try {
    return CheckList.create(req.body).then(data => res.status(201).send(data));
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    });
  }
});

router.get('/', async (req, res) => {
  try {
    let data = req.query.data;
    let placa = req.query.placa;

    let filtro = (data || placa) ? {
      $or: [
        data && {
          criado: { "$gte": toDate(data, '00:00:00'), "$lt": toDate(data, '23:59:59') }
        } || null,
        placa && { placa: placa.toUpperCase() } || null
      ].filter(e => e)
    } : {};

    const check = await CheckList.find(filtro)
      .populate('avaria')
      .populate('cliente')
      .populate('complemento')
      .populate('veiculo')
      .exec();

    return res.status(200).send(check);
  } catch (err) {
    console.log(err)
    return res.status(404).send({ error: 'Não possui registros cadastrados.' });
  }
});

router.put('/', (req, res) => {
  try {
    return Cliente.findOne({ _id: req.body._id }).then((result) => {
      if (!result) {
        return res.status(404).send({
          error: 'O Cliente informado não se encontra cadastrado.',
        });
      }
      Cliente.findOne({
        _id: { $ne: req.body._id },
        $or: [{ documento: req.body.documento }, { email: req.body.email }],
      }).then((result) => {
        if (result) {
          return res.status(409).send({
            error:
              'O Cliente informado já se encontra cadastrado, Verifique o documento e o e-mail.',
          });
        }

        Cliente.update({ _id: req.body._id }, req.body).then(cliente => res.status(200).send(cliente));
      });
    });
  } catch (err) {
    return res.status(400).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    return Cliente.findOne({ _id: req.params.id }).then((result) => {
      if (!result) {
        return res.status(404).send({
          error: 'O Cliente informado não se encontra cadastrado.',
        });
      }
      Cliente.deleteOne({ _id: req.params.id }).then(() => res.status(200).send());
    });
  } catch (err) {
    console.log(err);
    return res.status(404).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    });
  }
});

router.post('/email', async (req, res) => {

  var check = await CheckList.findOne({ _id: req.body.id })
    .populate('avaria')
    .populate('cliente')
    .populate('complemento')
    .populate('veiculo')
    .exec();

  const html = checkTemplate(check);

  const transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com",
    secureConnection: false,
    port: 587,
    tls: {
      ciphers: 'SSLv3'
    },
    auth: {
      user: 'teste.e.m.a.i.l@hotmail.com',
      pass: 'Dev*1424'
    }
  });

  const mailOptions = {
    from: 'teste.e.m.a.i.l@hotmail.com',
    to: req.body.email,
    subject: 'Check list',
    html: html
  };

  transporter.use('compile', inlineBase64({ cidPrefix: 'somePrefix_' }))

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.status(400).send({
        error: 'Não foi possivel enviar o email.',
        error
      });
    }
    return res.status(200).send(error);
  });

})
module.exports = app => app.use('/check-list', router);
