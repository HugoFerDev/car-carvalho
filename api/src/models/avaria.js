const mongoose = require('../database');

const AvariaSchema = new mongoose.Schema({
  descricaoLateralDireita: {
    type: String,
    uppercase: true
  },
  descricaoLateralEsquerda: {
    type: String,
    uppercase: true
  },
  descricaoTraseira: {
    type: String,
    uppercase: true
  },
  descricaoDianteira: {
    type: String,
    uppercase: true
  },
  descricaoTeto: {
    type: String,
    uppercase: true
  },
  descricaoOutros: {
    type: String,
    uppercase: true
  },
  observacao: {
    type: String,
    uppercase: true
  },
  imagemLateralDireita: [{ type: String }],
  imagemLateralEsquerda: [{ type: String }],
  imagemTraseira: [{ type: String }],
  imagemDianteira: [{ type: String }],
  imagemTeto: [{ type: String }],
  imagemOutros: [{ type: String }]
});

const Avaria = mongoose.model('Avaria', AvariaSchema, 'avaria');

module.exports = Avaria;
