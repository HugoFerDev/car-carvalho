const mongoose = require('../database');

const ComplementoSchema = new mongoose.Schema({
  pneus: {
    type: String,
  },
  pintura: {
    type: String,
  },
  tapecaria: {
    type: String,
  },
  combustivel: {
    type: String,
  },
  marcaBateria: {
    type: String,
  },
});

const Complemento = mongoose.model('Complemento', ComplementoSchema, 'complemento');

module.exports = Complemento;
