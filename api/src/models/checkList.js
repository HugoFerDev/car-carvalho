const mongoose = require('../database');

const Schema = mongoose.Schema;

const CheckListSchema = new mongoose.Schema({
  avaria: {
    type: Schema.Types.ObjectId,
    ref: 'Avaria',
  },
  cliente: {
    type: Schema.Types.ObjectId,
    ref: 'Cliente',
  },
  complemento: {
    type: Schema.Types.ObjectId,
    ref: 'Complemento',
  },
  veiculo: {
    type: Schema.Types.ObjectId,
    ref: 'Veiculo',
  },
  criado: {
    type: Schema.Types.Date,
    default: Date.now,
  },
  placa: {
    type: Schema.Types.String,
    uppercase: true
  }
});

const CheckList = mongoose.model('CheckList', CheckListSchema, 'checklist');

module.exports = CheckList;
