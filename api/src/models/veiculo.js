const mongoose = require('../database');

const VeiculoSchema = new mongoose.Schema({
  dvd: {
    type: Boolean,
  },
  tela: {
    type: Boolean,
    default: false
  },
  frente: {
    type: Boolean,
    default: false
  },
  tapete: {
    type: Boolean,
    default: false
  },
  manual: {
    type: Boolean,
    default: false
  },
  macaco: {
    type: Boolean,
    default: false
  },
  antena: {
    type: Boolean,
    default: false
  },
  calotas: {
    type: Boolean,
    default: false
  },
  radioCd: {
    type: Boolean,
    default: false
  },
  extintor: {
    type: Boolean,
    default: false
  },
  pneuStep: {
    type: Boolean,
    default: false
  },
  documento: {
    type: Boolean,
    default: false
  },
  triangulo: {
    type: Boolean,
    default: false
  },
  chaveRoda: {
    type: Boolean,
    default: false
  },
  rodaComum: {
    type: Boolean,
    default: false
  },
  acendedor: {
    type: Boolean,
    default: false
  },
  chaveReserva: {
    type: Boolean,
    default: false
  },
  rodaEspecial: {
    type: Boolean,
    default: false
  },
  carroFuncionando: {
    type: Boolean,
    default: false
  },
});

const Veiculo = mongoose.model('Veiculo', VeiculoSchema, 'veiculo');

module.exports = Veiculo;
