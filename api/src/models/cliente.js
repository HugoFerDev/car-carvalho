const mongoose = require('../database');

const Schema = mongoose.Schema;

const ClienteSchema = new mongoose.Schema({
  os: {
    type: String,
  },
  km: {
    type: String,
  },
  ano: {
    type: String,
  },
  cor: {
    type: String,
  },
  placa: {
    type: String,
    uppercase: true
  },
  marca: {
    type: String,
    uppercase: true
  },
  modelo: {
    type: String,
  },
  origem: {
    type: String,
  },
  remetente: {
    type: String,
  },
  enderecoRemetente: {
    type: String,
  },
  telefoneRemetente: {
    type: String,
  },
  destinatario: {
    type: String,
  },
  telefoneDestinatario: {
    type: String,
  },
  enderecoDestinatario: {
    type: String,
  },
  imagemVelocimetro: {
    type: String,
  },
});

const Cliente = mongoose.model('Cliente', ClienteSchema, 'cliente');

module.exports = Cliente;
