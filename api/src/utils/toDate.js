module.exports = function (date, time) {
  let dataSplit = date.split('-');
  let timeSplit = time.split(':');

  return new Date(dataSplit[0], parseInt(dataSplit[1]) - 1, dataSplit[2], timeSplit[0], timeSplit[1], timeSplit[2])
}