import React from 'react';

const MessageError = ({message}) => {

  return (
    <div>
      <span style={{color: 'red'}}>{message}</span>
    </div>
  )
}

export default MessageError;
