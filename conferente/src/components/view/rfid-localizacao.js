import {
  Button,
  Card,
  Carousel,
  Col,
  Container,
  Form,
  Row,
  Spinner,
} from "react-bootstrap";
import React, { Component } from "react";

import Page from "./page";
import Util from "../../utils/util";
import { addConferente } from "../../actions/conferente";
import { saveRfidLocalization } from "../../actions/check-list";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFormik } from "formik";

class RfidLocalizacao extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {};

    this.getLocation = this.getLocation.bind(this);
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue, values } = this.props;
    const { files } = event.target;
    const imagens = [];
    for (let index = 0; index < files.length; index++) {
      imagens.push(Util.readUploadedFileAsText(files[index]));
    }

    Promise.all(imagens).then((data) => {
      const res = values[item];
      if (res.length >= 10) return;
      data.forEach((element) => {
        res.push(element);
      });

      setFieldValue(`${item}`, res);

      let check = localStorage.getItem('checklist');
      check = JSON.parse((check  || '{}'));
      check = {...check, conferente: values};

      localStorage.setItem('checklist', JSON.stringify(check));
    });
  }

  getLocation () {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        this.props.setFieldValue('latitude', latitude);
        this.props.setFieldValue('longitude', longitude);
        alert('Localização Registrada');
      },
      () => { },
      {
        timeout: 3000,
        enableHighAccuracy: true,
      }
    )
  }

  render() {
    const { values, handleSubmit, handleChange, conferente } = this.props;

    if(conferente.loading) {
      return (
        <div
          className="d-flex align-items-center justify-content-center"
          style={{ height: '100vh' }}
        >
          <div className="p-4 rounded" style={{ backgroundColor: '#fff' }}>
            <Spinner animation="border" variant="primary" />
          </div>
        </div>
      );
    }

    return (
      <Page title="DADOS CONFERENTE" pageBack="avaria">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card className="mt-2">
                    <Card.Body className="shadow rounded">

                      <input
                        id="imagemMotor"
                        style={{ display: "none" }}
                        type="file"
                        name="imagemMotor"
                        accept="image/*"
                        capture
                        onChange={(event) =>
                          this.setImagem("imagemMotor", event)
                        }
                      />

                      <Form.Group>
                        <Form.Label>RFID/IDENTIFICADOR:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.rfidIdentificador}
                          onChange={handleChange}
                          name="rfidIdentificador"
                        />
                      </Form.Group>


                      <Button
                        onClick={this.getLocation}
                        className="w-100 mt-4"
                        variant="primary"
                      >
                        CAPTURAR LOCALIZAÇÃO
                      </Button>

                      <Button
                        onClick={handleSubmit}
                        className="w-100 mt-4"
                        variant="primary"
                      >
                        Gravar Dados
                      </Button>
                    </Card.Body>
                  </Card>

                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { conferente: state.conferente, check: state };
}

export default compose(
  connect(mapStateToProps, { addConferente, saveRfidLocalization }),
  withFormik({
    enableReinitialize: true,
    mapPropsToValues: (props) => props.conferente,

    handleSubmit: (values, { props }) => {
      let ifConnected = window.navigator.onLine;
      if (!ifConnected) {
        alert('Sem conexão com a Internet, Verifique!');
        return;
      }

      let check = props.check;
      check = Object.assign(
        {
          ...check,
          cliente: {
            ...check.cliente,
            chamadoId: localStorage.getItem("chamado_id"),
          },
        },
        { conferente: values },
        {acesso: localStorage.getItem('acesso')}
      );
      props.saveRfidLocalization(check)
      .then(() => {
        props.history.push("/menu");
      })
      .catch(err => {
        alert('Houve um problema, tente novamente mais tarde!' + err)
      })
    },
  })
)(RfidLocalizacao);
