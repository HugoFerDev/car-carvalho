import * as checklist_action from "../../actions/check-list";

import { Button, Container, Form, Row } from "react-bootstrap";
import React, { Fragment, useEffect, useState } from "react";

import Page from "./page";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import history from "../../utils/history";
import MessageError from "../../shared/MessageError";

const Menu = ({
  loading_autoridade,
  success_autoridade,
  get_checklist_request,
  reset_autoridade,
}) => {
  const [placa, setPlaca] = useState();
  const [errorPlaca, setErrorPlaca] = useState();

  useEffect(() => {
    if (!loading_autoridade && success_autoridade) {
      let chamado = JSON.parse(localStorage.getItem("chamado"));
      reset_autoridade();
      history.push(`/chamado/${chamado.insertId}`);
    }
  }, [loading_autoridade, success_autoridade]);

  const getCheckList = () => {
    if (placa && placa.length > 0 ) {
      get_checklist_request(placa);
      setErrorPlaca();
    } else {
      setErrorPlaca("Digite NCV ou PLACA");
    }
  };

  return (
    <Page title="OPÇÕES" pageBack="">
      <div className="d-flex flex-column justify-content-between h-80vh">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Fragment>
                  <Form.Group className="w-100">
                    <Form.Label>Informe Placa ou NCV</Form.Label>
                    <Form.Control
                      onChange={(e) => setPlaca(e.target.value)}
                      type="text"
                      className="transform-uppercase w-100"
                      name="ncv"
                    />
                    {errorPlaca ? (
                      <MessageError message={errorPlaca} />
                    ) : null}
                  </Form.Group>

                  <Button
                    onClick={getCheckList}
                    className="w-100 mt-3"
                    variant="primary"
                  >
                    BUSCAR
                  </Button>
                </Fragment>
              </Row>
            </Form>
          </Container>
        </div>
      </div>
    </Page>
  );
};

function mapStateToProps({ autoridade }) {
  return {
    loading_autoridade: autoridade.loading,
    success_autoridade: autoridade.success,
  };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...checklist_action }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
