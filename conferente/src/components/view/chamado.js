import "leaflet-offline";

import * as chamado_action from "../../actions/chamado";

import { Badge, Button, Container, Row, Spinner } from "react-bootstrap";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import React, { Fragment, useEffect, useState } from "react";

import L from "leaflet";
import Page from "./page";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import enviroment from "../../enviroment";
import localforage from "localforage";

const Chamado = ({ match, chamado, get_chamado_request }) => {
  const [location, setLocation] = useState({
    region: { lat: null, lng: null },
    zoom: 18
  });

  const initializingMap = () => {
    let container = L.DomUtil.get("map-id");
    if (container != null) {
      container._leaflet_id = null;
    }
  };

  useEffect(() => {
    get_chamado_request(match.params.id);
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      get_chamado_request(match.params.id);
    }, enviroment.TIME_REFRESH);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (chamado.latitude_atendimento) {
      setLocation({
        ...location,
        region: {
          lat: chamado.latitude_atendimento,
          lng: chamado.longitude_atendimento
        }
      });
      initializingMap();
      const map = L.map("map-id");
      const offlineLayer = L.tileLayer.offline(
        "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        localforage,
        {
          attribution:
            '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
          subdomains: "abc",
          minZoom: 10,
          maxZoom: 20,
          crossOrigin: true
        }
      );
      offlineLayer.addTo(map);
    }
  }, [chamado]);

  return (
    <Page title="ULTIMA LOCALIZAÇÃO" pageBack="menu">
      <div>
        {chamado.latitude_atendimento ? (
          <Fragment>
            <div id="map-id" style={{ height: "60vh" }}>
              <Map
                center={location.region}
                zoom={location.zoom}
                maxZoom={30}
                id="map"
              >
                <TileLayer
                  attribution='&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
                />
                <Marker position={location.region}></Marker>
              </Map>
            </div>

            <div className="d-flex flex-column">
              <div className="d-flex flex-column">
                <div>Localização: </div>
                <span>
                  {chamado.endereco &&
                    chamado.endereco.address &&
                    `${chamado.endereco.address.road}, ${chamado.endereco.address.city_district}, ${chamado.endereco.address.state}`}
                </span>
              </div>
              <div className="d-flex flex-column">
                <div>Última Atualização: </div>
                <span>
                  {chamado.data_hora_localizacao}
                </span>
              </div>
            </div>
          </Fragment>
        ) : (
            <Container className="mt-4">
              <Row className="justify-content-center">
                <Badge
                  variant="primary"
                  className="d-flex justify-content-center align-items-center overflow-hidden"
                >
                  <Spinner className="mr-2" animation="border" role="status" />
                  Aguardando o chamado ser aceito...
              </Badge>
              </Row>
            </Container>
          )}
      </div>
    </Page>
  );
};

function mapStateToProps({ chamado }) {
  return {
    chamado: chamado.chamados.length ? chamado.chamados[0] : {}
  };
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...chamado_action }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chamado);
