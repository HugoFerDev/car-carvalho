import * as login_action from "../../actions/login";

import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import React, { Component } from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import image from "../../assets/img/thumbnail_logo.jpg";
import { withRouter } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: "",
      senha: "",
    };
    this.login = this.login.bind(this);
  }

  login() {
    const { login_request, history } = this.props;
    const { login, senha } = this.state;

    login_request(login, senha)
      .then(data => {
        localStorage.setItem(
          "acesso",
          JSON.stringify({
            tipo: data.tipousuario.split(" ")[1],
            motorista: data.idUsuario,
            autoridade: data.idUsuario,
            nome: data.nomeUsuario,
            token: data.token
          })
        );

        if(data.tipousuario.split(" ")[1] === 'autoridade') {
          history.push("/menu");
        } else {
          alert("Usuário não é do tipo autoridade!");
        }
      })
      .catch(err => {
        alert("Usuário ou senha inválida!");
      });
  }

  render() {
    return (
      <div className="bg-primary min-vh-100 d-flex align-items-center">
        <Container className="container-fluid">
          <Row className="d-flex justify-content-center">
            <Col className="col-lg-7">
              <Card className="min">
                <div className="text-center">
                  <Card.Img className="w-50" variant="top" src={image} />
                </div>
                <Card.Body>
                  <Form>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Control
                        onChange={(e) =>
                          this.setState({ login: e.target.value })
                        }
                        type="text"
                        placeholder="Usuário"
                      />
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                      <Form.Control
                        onChange={(e) =>
                          this.setState({ senha: e.target.value })
                        }
                        type="password"
                        placeholder="Senha"
                      />
                    </Form.Group>
                    <Button
                      className="w-100"
                      variant="primary"
                      type="button"
                      onClick={this.login}
                    >
                      Login
                    </Button>
                  </Form>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...login_action }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
