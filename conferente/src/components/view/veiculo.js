import { compose } from "redux";
import { withFormik } from "formik";
import { connect } from "react-redux";
import React, { Component } from "react";

import { Container, Row, Col, Card, Form, Button } from "react-bootstrap";
import Page from "./page";
import { addVeiculo } from "../../actions/veiculo";
import Util from "../../utils/util";

class Veiculo extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {};
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue } = this.props;

    const file = event.target.files[0];
    setFieldValue(`${item}`, await Util.readUploadedFileAsText(file));
  }

  render() {
    const { values, handleChange, handleSubmit } = this.props;

    const options = [
      {
        value: "arCondicionado",
        label: "Ar-Condicionado",
      },
      {
        value: "vidroEletrico",
        label: "Vidro Elétrico",
      },
      {
        value: "cambioManual",
        label: "Cámbio Manual",
      },
      {
        value: "cambioAutomatico",
        label: "Cámbio Automático",
      },
      {
        value: "acendedor",
        label: "Acendedor",
      },
      {
        value: "radioCd",
        label: "Radio/CD",
      },
      {
        value: "frente",
        label: "Frente",
      },
      {
        value: "tela",
        label: "Tela",
      },
      {
        value: "extintor",
        label: "Extintor",
      },
      {
        value: "pneuStep",
        label: "Pneu Step",
      },
      {
        value: "macaco",
        label: "Macaco",
      },
      {
        value: "rodaComum",
        label: "Roda Comum",
      },
      {
        value: "rodaEspecial",
        label: "Roda Especial",
      },
      {
        value: "calotas",
        label: "Calotas",
      },
      {
        value: "documento",
        label: "Documento",
      },
      {
        value: "carroFuncionando",
        label: "Carro Funcionando",
      },
    ];

    return (
      <Page title="ACESSÓRIOS" pageBack="cliente">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card>
                    <Card.Body className="shadow rounded">
                      <div className="d-flex justify-content-between flex-wrap">
                        <div className="d-flex flex-column">
                          {options.map((op) => (
                            <div className="d-flex">
                              <div className="round mr-5">
                                <Form.Control
                                  type="checkbox"
                                  className="round"
                                  id={`input${op.value}`}
                                  onChange={handleChange}
                                  name={op.value}
                                  checked={values[op.value]}
                                />
                                <Form.Label htmlFor={`input${op.value}`} />
                              </div>
                              <Form.Label
                                className="cursor-pointer"
                                htmlFor={`input${op.value}`}
                              >
                                {op.label}
                              </Form.Label>
                            </div>
                          ))}
                        </div>
                      </div>

                      <Button
                        onClick={handleSubmit}
                        className="w-100 mt-4"
                        variant="primary"
                      >
                        Proximo
                      </Button>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { veiculo: state.veiculo };
}

export default compose(
  connect(mapStateToProps, { addVeiculo }),
  withFormik({
    enableReinitialize: true,
    mapPropsToValues: (props) => props.veiculo,

    handleSubmit: (values, { props }) => {
      props.addVeiculo(values);
      props.history.push("/complemento");
    },
  })
)(Veiculo);
