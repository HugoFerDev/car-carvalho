import * as checklist_action from "../../actions/check-list";

import { Button, Container, Form, Row } from "react-bootstrap";
import React, { Fragment, useEffect, useState } from "react";

import Page from "./page";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import history from "../../utils/history";

const OptionMenu = ({
  loading_autoridade,
  success_autoridade,
  get_checklist_request,
  reset_autoridade,
}) => {
  const [placa, setPlaca] = useState();
  const [errorPlaca, setErrorPlaca] = useState();

  useEffect(() => {
    if (!loading_autoridade && success_autoridade) {
      let chamado = JSON.parse(localStorage.getItem("chamado"));
      reset_autoridade();
      history.push(`/chamado/${chamado.insertId}`);
    }
  }, [loading_autoridade, success_autoridade]);

  const getCheckList = () => {
    if (placa && placa.length > 0 ) {
      get_checklist_request(placa);
      setErrorPlaca();
    } else {
      setErrorPlaca("Digite NCV ou PLACA");
    }
  };

  return (
    <Page title="OPÇÕES" pageBack="">
      <div className="d-flex flex-column justify-content-between h-80vh">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Fragment>
                  <Button
                    onClick={() => history.push('/cliente')}
                    className="w-100 mt-3"
                    variant="primary"
                  >
                    CONFERÊNCIA
                  </Button>
                  <Button
                    onClick={() => history.push('/rfid-localizacao')}
                    className="w-100 mt-3"
                    variant="primary"
                  >
                    RFID/LOCALIZAÇÃO
                  </Button>
                </Fragment>
              </Row>
            </Form>
          </Container>
        </div>
      </div>
    </Page>
  );
};

function mapStateToProps({ autoridade }) {
  return {
    loading_autoridade: autoridade.loading,
    success_autoridade: autoridade.success,
  };
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...checklist_action }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OptionMenu);
