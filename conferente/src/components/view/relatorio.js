import React, { Component } from "react";

import {
  Container,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Alert
} from "react-bootstrap";
import Table from "react-bootstrap/Table";
import { connect } from "react-redux";
import { withFormik } from "formik";
import { compose } from "redux";

import Page from "./page";
// import { fetchCheckList, sendEmail } from "../../actions/check-list";
import Util from "../../utils/util";

class Relatorio extends Component {
  static sender = "sender@example.com";
  constructor(props) {
    super(props);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.sendEmail = this.sendEmail.bind(this);

    this.state = {
      show: false,
      alert: { open: false, message: "" },
      checklist: {},
      email: ""
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow(check) {
    this.setState({ show: true, checklist: check, email: "", alert: {} });
  }

  sendEmail() {
    const { email, checklist } = this.state;

    if (email) {
      this.props.sendEmail(checklist._id, email);
      this.setState({
        alert: {
          open: true,
          message: "E-mail encaminhado com sucesso",
          type: "success"
        }
      });
    } else {
      this.setState({
        alert: {
          open: true,
          message: "Informe um e-mail para envio",
          type: "danger"
        }
      });
    }
  }

  render() {
    const { checklists, handleChange, handleSubmit } = this.props;

    console.log(checklists);
    return (
      <Page title="Relatório" pageBack="menu">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Row>
              <Col md={3}>
                <Form.Group>
                  <Form.Label>Data:</Form.Label>
                  <Form.Control
                    type="date"
                    onChange={handleChange}
                    className="transform-uppercase form-control-sm"
                    name="data"
                  />
                </Form.Group>
              </Col>

              <Col md={3}>
                <Form.Group>
                  <Form.Label>Placa:</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={handleChange}
                    className="transform-uppercase form-control-sm"
                    name="placa"
                  />
                </Form.Group>
              </Col>

              <Col md={2}>
                <Button
                  className="w-100 mt-4"
                  variant="primary"
                  onClick={handleSubmit}
                >
                  Buscar
                </Button>
              </Col>
            </Row>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Criado</th>
                  <th>Remetente</th>
                  <th>Destinatário</th>
                  <th>Placa</th>
                  <th>Ano</th>
                  <th>Cor</th>
                  <th>Ação</th>
                </tr>
              </thead>
              <tbody>
                {checklists.map((check, index) => (
                  <tr key={index}>
                    <td>{Util.formatDate(check.criado)}</td>
                    <td>{check.cliente.remetente}</td>
                    <td>{check.cliente.destinatario}</td>
                    <td>{check.cliente.placa}</td>
                    <td>{check.cliente.ano}</td>
                    <td>{check.cliente.cor}</td>
                    <td>
                      <i
                        className="fa fa-search cursor-pointer"
                        onClick={() => this.handleShow(check)}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Container>
        </div>

        <Modal show={this.state.show} onHide={this.handleClose} size="lg">
          <Alert
            show={this.state.alert.open}
            dismissible
            variant={this.state.alert.type}
          >
            {this.state.alert.message}
          </Alert>
          <Modal.Header closeButton>
            <Modal.Title>Detalhes Check-list</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Form.Control
              type="email"
              placeholder="E-mail"
              className="w-50"
              value={this.state.email}
              onChange={({ target: { value } }) =>
                this.setState({ email: value })
              }
              name="marca"
            />

            <Button variant="primary" onClick={this.sendEmail}>
              Enviar e-mail
            </Button>
            <Button variant="secondary" onClick={this.handleClose}>
              Fechar
            </Button>
          </Modal.Footer>
        </Modal>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { checklists: state.checklist.checklists };
}

export default compose(
  connect(mapStateToProps, {  }),
  withFormik({
    mapPropsToValues: () => ({ data: "", placa: "" }),

    handleSubmit: (values, { props }) => {
      // props.fetchCheckList(values);
    }
  })
)(Relatorio);
