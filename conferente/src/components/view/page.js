import * as checklist_action from "../../actions/check-list";

import React, { Component } from "react";
import { Container, Row, Col, Navbar } from "react-bootstrap";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { bindActionCreators, compose } from "redux";
import { connect } from "react-redux";

class Page extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    title: PropTypes.node.isRequired,
    pageBack: PropTypes.node.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    this.props.setChecklistLocal();
  }

  render() {
    const { title, children, pageBack, history } = this.props;

    return (
      <div>
        <Container className="min-vh-100 shadow bg-white rounded">
          <Row>
            <Col className="p-0">
              <Navbar className="bg-primary">
                <Navbar.Brand className="cl-white">
                  {pageBack ? (
                    <i
                      className="fa fa-arrow-left cl-white"
                      onClick={() => {
                        history.push(`/${pageBack}`);
                      }}
                      aria-hidden="true"
                    />
                  ) : null}
                </Navbar.Brand>

                <div className="w-100 cl-white font-weight-bold text-center">
                  <span style={{ fontSize: "1.6em" }}>{title}</span>
                </div>
              </Navbar>
            </Col>
          </Row>

          <div className="content-centro">{children}</div>
        </Container>
      </div>
    );
  }
}

function mapStateToProps() {
  return {};
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...checklist_action }, dispatch);

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(Page);
