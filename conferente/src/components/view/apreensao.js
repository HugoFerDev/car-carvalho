import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import React, { Component } from "react";
import { addApreensao } from "../../actions/apreensao";

import InputMask from "react-input-mask";
import Page from "./page";
import Util from "../../utils/util";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFormik } from "formik";

class Apreensao extends Component {
  /**
   * Constructor da Classe.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    this.setImagem = this.setImagem.bind(this);
  }

  async componentDidMount() {
    const { setFieldValue } = this.props;
    setFieldValue("os", await Util.generateOS());
  }

  /**
   * Método responsável por ler a imagem informada e setar na state.
   *
   * @param item
   * @param event
   */
  async setImagem(item, event) {
    const { setFieldValue, values } = this.props;

    if (item == "veiculo") {
      const { files } = event.target;
      const imagens = values.imagens.veiculo;
      for (let index = 0; index < files.length; index++) {
        imagens.push(Util.readUploadedFileAsText(files[index]));
      }

      Promise.all(imagens).then((values) => {
        setFieldValue(`imagens.${item}`, values);
      });
    } else {
      const file = event.target.files[0];
      setFieldValue(`imagens.${item}`, await Util.readUploadedFileAsText(file));
    }
  }

  consultarPlaca(value) {
    const { buscarPlaca, setFieldValue } = this.props;
    // buscarPlaca(value).then((carro) => {
    //   setFieldValue("ano", carro.ano);
    //   setFieldValue("marca", carro.marca);
    //   setFieldValue("modelo", carro.modelo);
    //   setFieldValue("chassi", carro.chassi);
    //   setFieldValue("cor", carro.cor);
    // });
  }

  render() {
    const { values, handleChange, handleSubmit } = this.props;
    const { imagens } = values;

    return (
      <Page title="Dados de Apreensão" pageBack="cliente">
        <div className="d-flex align-items-center mt-4">
          <Container>
            <Form>
              <Row className="d-flex justify-content-center">
                <Col className="col-lg-7">
                  <Card className="min">
                    <Card.Body className="shadow rounded">
                      <Form.Group>
                        <Form.Label>Ordem de Serviço:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.ordemServico}
                          onChange={handleChange}
                          name="ordemServico"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Chaves:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.chaves ? 'Sim' : 'Não'}
                          onChange={handleChange}
                          name="chaves"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Blitz:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.blitz ? 'Sim' : 'Não'}
                          onChange={handleChange}
                          name="blitz"
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>KM Percorrido:</Form.Label>
                        <Form.Control
                          type="text"
                          className="transform-uppercase"
                          value={values.kmPercorrido}
                          onChange={handleChange}
                          name="kmPercorrido"
                        />
                      </Form.Group>

                      <div className="d-flex">
                        <div className="round mr-5">
                          <Form.Control
                            type="checkbox"
                            className="round"
                            id="guinchoColetivo"
                            onChange={handleChange}
                            name="guinchoColetivo"
                            checked={values.guinchoColetivo}
                          />
                          <Form.Label htmlFor="guinchoColetivo" />
                        </div>
                        <Form.Label
                          className="cursor-pointer"
                          htmlFor="guinchoColetivo"
                        >
                          Guincho Coletivo
                        </Form.Label>
                      </div>

                      <Form.Group>
                        <Form.Label>Motivos da Apreensão:</Form.Label>
                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputAdulterado"
                              onChange={handleChange}
                              name="adulterado"
                              checked={values.adulterado}
                            />
                            <Form.Label htmlFor="inputAdulterado" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputAdulterado"
                          >
                            Adulterado
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputCrimesTransito"
                              onChange={handleChange}
                              name="crimesTransito"
                              checked={values.crimesTransito}
                            />
                            <Form.Label htmlFor="inputCrimesTransito" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputCrimesTransito"
                          >
                            Crimes de Trânsito
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputEmTela"
                              onChange={handleChange}
                              name="emTela"
                              checked={values.emTela}
                            />
                            <Form.Label htmlFor="inputEmTela" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputEmTela"
                          >
                            Em Tela
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputForaCirculacao"
                              onChange={handleChange}
                              name="foraCirculacao"
                              checked={values.foraCirculacao}
                            />
                            <Form.Label htmlFor="inputForaCirculacao" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputForaCirculacao"
                          >
                            Fora de Circulação
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputJudicial"
                              onChange={handleChange}
                              name="judicial"
                              checked={values.judicial}
                            />
                            <Form.Label htmlFor="inputJudicial" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputJudicial"
                          >
                            Judicial
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputLeasing"
                              onChange={handleChange}
                              name="leasing"
                              checked={values.leasing}
                            />
                            <Form.Label htmlFor="inputLeasing" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputLeasing"
                          >
                            Leasing
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputInfracao"
                              onChange={handleChange}
                              name="infracao"
                              checked={values.infracao}
                            />
                            <Form.Label htmlFor="inputInfracao" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputInfracao"
                          >
                            Infração
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputMotorComQueixa"
                              onChange={handleChange}
                              name="motorComQueixa"
                              checked={values.motorComQueixa}
                            />
                            <Form.Label htmlFor="inputMotorComQueixa" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputMotorComQueixa"
                          >
                            Motor Com Queixa
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputPedirBaixa"
                              onChange={handleChange}
                              name="pedirBaixa"
                              checked={values.pedirBaixa}
                            />
                            <Form.Label htmlFor="inputPedirBaixa" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputPedirBaixa"
                          >
                            Pedir Baixa
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputPolicialCivil"
                              onChange={handleChange}
                              name="policialCivil"
                              checked={values.policialCivil}
                            />
                            <Form.Label htmlFor="inputPolicialCivil" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputPolicialCivil"
                          >
                            Policial Civíl
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputTraficoDrogas"
                              onChange={handleChange}
                              name="traficoDrogas"
                              checked={values.traficoDrogas}
                            />
                            <Form.Label htmlFor="inputTraficoDrogas" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputTraficoDrogas"
                          >
                            Tráfico Drogas
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputRouboFurto"
                              onChange={handleChange}
                              name="rouboFurto"
                              checked={values.rouboFurto}
                            />
                            <Form.Label htmlFor="inputRouboFurto" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputRouboFurto"
                          >
                            Roubo/Furto
                          </Form.Label>
                        </div>

                        <div className="d-flex">
                          <div className="round mr-5">
                            <Form.Control
                              type="checkbox"
                              className="round"
                              id="inputSemDocumentoCrv"
                              onChange={handleChange}
                              name="semDocumentoCrv"
                              checked={values.semDocumentoCrv}
                            />
                            <Form.Label htmlFor="inputSemDocumentoCrv" />
                          </div>
                          <Form.Label
                            className="cursor-pointer"
                            htmlFor="inputSemDocumentoCrv"
                          >
                            SEM DOCUMENTO/CRV
                          </Form.Label>
                        </div>
                      </Form.Group>

                      <Button
                        className="w-100 mt-4"
                        variant="primary"
                        onClick={handleSubmit}
                      >
                        Proximo
                      </Button>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Form>
          </Container>
        </div>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return { apreensao: state.apreensao };
}

export default compose(
  connect(mapStateToProps, { addApreensao }),
  withFormik({
    enableReinitialize: true,
    mapPropsToValues: (props) => props.apreensao,

    handleSubmit: (values, { props }) => {
      props.addApreensao(values);
      props.history.push("/veiculo");
    },
  })
)(Apreensao);
