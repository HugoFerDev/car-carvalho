import {ADD_CLIENTE, BUSCAR_PLACA} from './types';

export function addCliente(cliente) {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));
  check = {...check, cliente};
  localStorage.setItem('checklist', JSON.stringify(check));

  return {
    type: ADD_CLIENTE,
    cliente,
  };
}
