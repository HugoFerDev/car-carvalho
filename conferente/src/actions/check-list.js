import {CHECKLIST_SAVED_FAILED, CHECKLIST_SAVED_SUCCESS, RESET_STATE} from './types';

import enviroment from '../enviroment';
import request from '../utils/request';
import { addApreensao } from './apreensao';
import { addVeiculo } from './veiculo';
import { addAvariaCheck } from './avaria';
import { addComplemento } from './complemento';
import history from './../utils/history';
import { addCliente } from './cliente';
import { addConferenteLocal } from './conferente';

export const get_checklist_request = (placa) => (dispatch) => {
  request(`${enviroment.apiAmazon}/checklists/dados-conferente/${placa}`)
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }
      throw response;
    })
    .then((data) => {
      dispatch(addVeiculo({
        ...data.acessorios[0],
        rodaEspecial: data.acessorios[0].rodaespecial,
        rodaComum: data.acessorios[0].rodacomum,
        chaveReserva: data.acessorios[0].chavereserva,
        pneuStep: data.acessorios[0].pneustep,
        radioCd: data.acessorios[0].radiocd,
        chaveRoda: data.acessorios[0].chaveroda,
        carroFuncionando: data.acessorios[0].carrofuncionando,
      }));
      dispatch(addApreensao({
        ...data.apreensao[0],
        emTela: data.apreensao[0].cb_emtela,
        adulterado: data.apreensao[0].cb_adulterado,
        crimesTransito: data.apreensao[0].cb_crimestransito,
        foraCirculacao: data.apreensao[0].cb_foracirculacao,
        infracao: data.apreensao[0].cb_infracao,
        judicial: data.apreensao[0].cb_judicial,
        leasing: data.apreensao[0].cb_leasing,
        motorComQueixa: data.apreensao[0].cb_motorqueixa,
        pedirBaixa: data.apreensao[0].cb_pedirbaixa,
        policialCivil: data.apreensao[0].cb_policialcivil,
        rouboFurto: data.apreensao[0].cb_roubofurto,
        semDocumentoCrv: data.apreensao[0].cb_semdocumentocrv,
        traficoDrogas: data.apreensao[0].cb_traficodedrogas,
        ordemServico: data.apreensao[0].cb_traficodedrogas,
      }));

      dispatch(addAvariaCheck(data.avarias.map(e => ({...e, imagens: 'https://s3-sa-east-1.amazonaws.com/fotos.checklist/' + e.imagens}))));

      dispatch(addComplemento({
        ...data.complemento[0],
        pneus: data.complemento[0].estadopneus,
        pintura: data.complemento[0].estadopintura,
        tapecaria: data.complemento[0].estadotapecaria,
        combustivel: 'https://s3-sa-east-1.amazonaws.com/fotos.checklist/' + data.complemento[0].imagem_combustivel,
        marcaBateria: data.complemento[0].marcabateria,
      }));

      dispatch(addCliente({
        ...data.ncv[0],
        modelo: data.ncv[0].marca_modelo,
        km: data.complemento[0].km
      }));

      history.push('/option-menu')
    });
};

export const saveChecklistRequest = (check) => (dispatch) => new Promise((res, rej) => {
  request(`${enviroment.apiAmazon}/checklists/grava-conferente`, 'POST', check)
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }
      throw response
    })
    .then(() => {
      dispatch({type: RESET_STATE});
      res('saved')
      dispatch({type: CHECKLIST_SAVED_SUCCESS});
    })
    .catch(() => {
      dispatch({type: CHECKLIST_SAVED_FAILED});
      rej('falha')
    })
})

export const saveRfidLocalization = (check) => (dispatch) => new Promise((res, rej) => {
  request(`${enviroment.apiAmazon}/checklist/rfid-localizacao`, 'POST', check)
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }
      throw response
    })
    .then(() => {
      dispatch({type: RESET_STATE});
      res('saved')
      dispatch({type: CHECKLIST_SAVED_SUCCESS});
    })
    .catch(() => {
      dispatch({type: CHECKLIST_SAVED_FAILED});
      rej('falha')
    })
})

export const setChecklistLocal = () => (dispatch) => {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));

  const { apreensao, veiculo, avaria, complemento, cliente, conferente } = check;

  if(apreensao) dispatch(addApreensao(apreensao));
  if(veiculo) dispatch(addVeiculo(veiculo));
  if(avaria) dispatch(addAvariaCheck(avaria));
  if(complemento) dispatch(addComplemento(complemento));
  if(cliente) dispatch(addCliente(cliente));
  if(conferente) dispatch(addConferenteLocal(conferente));
}

export const checkSaved = () => (dispatch) => {
  dispatch({type: RESET_STATE});
};
