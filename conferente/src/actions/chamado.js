import request from '../utils/request';
import enviroment from '../enviroment';

export const get_chamado_request = id => async dispatch => {
  request(`${enviroment.apiAmazon}/chamados/pega-localizacao/${id}`)
    .then(response => response.json())
    .then(data => {
      (async () => {
        for (let i = 0; i < data.length; i++) {
          data[i].endereco = await get_address_request(
            data[i].latitude_atendimento,
            data[i].longitude_atendimento,
          );
        }
        dispatch({
          type: 'GET_CHAMADO_SUCCESS',
          response: data,
        });
      })();
    });
};

export const accept_chamado_request = chamado => dispatch => {
  return new Promise((res, rej) => {
    request(`${enviroment.apiAmazon}/chamados/aceite-chamado`, 'POST', chamado)
      .then(response => {
        if (response.status == 200) {
          return response;
        }
        rej('falha');
        throw response;
      })
      .then(() => {
        res('Sucesso');
      })
      .catch(() => {
        rej('falha');
      });
  });
};

export const update_location_chamado_request = chamado => dispatch => {
  request(`${enviroment.apiAmazon}/chamados/envia-localizacao`, 'POST', chamado)
    .then(response => response.json())
    .then(data => {});
};


export const get_chamados_by_autoridade_request = id => async dispatch => {
  request(
    `${
      enviroment.apiAmazon
    }/chamados/busca-chamados-autoridade?idautoridade=${id}`,
  )
    .then(response => response.json())
    .then(data => {
      dispatch({
        type: 'GET_CHAMADO_SUCCESS',
        response: data,
      });
    });
};

const get_address_request = async (lat, lon) => {
  const response = await fetch(
    `https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lon}&format=json`,
  );

  if (!response.ok) return {};
  const data = await response.json();

  return data;
};
