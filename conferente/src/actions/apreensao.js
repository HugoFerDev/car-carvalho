import { ADD_APREENSAO } from './types';

export function addApreensao(apreensao) {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));
  check = {...check, apreensao};
  localStorage.setItem('checklist', JSON.stringify(check));

  return {
    type: ADD_APREENSAO,
    apreensao,
  };
}
