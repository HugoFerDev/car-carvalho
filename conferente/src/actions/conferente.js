import { ADD_CONFERENTE, ADD_CONFERENTE_LOCAL } from './types';

export function addConferente(conferente) {
  return {
    type: ADD_CONFERENTE,
    conferente,
  };
}

export function addConferenteLocal(conferente) {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));
  check = {...check, conferente};
  localStorage.setItem('checklist', JSON.stringify(check));

  return {
    type: ADD_CONFERENTE_LOCAL,
    conferente,
  };
}
