import { ADD_COMPLEMENTO } from './types';

export function addComplemento(complemento) {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));
  check = {...check, complemento};
  localStorage.setItem('checklist', JSON.stringify(check));

  return {
    type: ADD_COMPLEMENTO,
    complemento,
  };
}
