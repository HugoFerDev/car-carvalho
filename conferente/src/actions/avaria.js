import { ADD_AVARIA, ADD_AVARIA_CHECK } from './types';

export function addAvaria(avaria) {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));
  check = {...check, avaria};
  localStorage.setItem('checklist', JSON.stringify(check));

  return {
    type: ADD_AVARIA,
    avaria,
  };
}
export function addAvariaCheck(avaria) {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));
  check = {...check, avaria};
  localStorage.setItem('checklist', JSON.stringify(check));

  return {
    type: ADD_AVARIA_CHECK,
    response: avaria,
  };
}
