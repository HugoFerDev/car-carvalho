import { ADD_VEICULO } from './types';

export function addVeiculo(veiculo) {
  let check = localStorage.getItem('checklist');
  check = JSON.parse((check  || '{}'));
  check = {...check, veiculo};
  localStorage.setItem('checklist', JSON.stringify(check));

  return {
    type: ADD_VEICULO,
    veiculo,
  };
}
