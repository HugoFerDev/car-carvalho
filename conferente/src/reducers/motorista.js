const initialState = {
  motoristas: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_MOTORISTAS_SUCCESS': {
      return {
        ...state,
        motoristas: action.response
      }
    }

    default: {
      return state;
    }
  }
};
