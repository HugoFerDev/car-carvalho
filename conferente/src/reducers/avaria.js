import {ADD_AVARIA,ADD_AVARIA_CHECK, RESET_STATE} from '../actions/types';

const initialState = {
  descricaoLateralDireita: '',
  imagemLateralDireita: [],

  descricaoLateralEsquerda: '',
  imagemLateralEsquerda: [],

  descricaoTraseira: '',
  imagemTraseira: [],

  descricaoDianteira: '',
  imagemDianteira: [],

  descricaoTeto: '',
  imagemTeto: [],

  descricaoOutros: '',
  imagemOutros: [],
  documentos: [],
  avarias: [],

  observacao: '',
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_AVARIA: {
      const {avaria} = action;
      return Object.assign({}, state, {...avaria, loading: true});
    }

    case ADD_AVARIA_CHECK: {
      const {response} = action;
      return Object.assign({}, state, {avarias: response});
    }

    case RESET_STATE: {
      return initialState;
    }

    default: {
      return state;
    }
  }
};
