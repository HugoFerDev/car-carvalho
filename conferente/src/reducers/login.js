const initialState = {
  loading: false,
  success: false,
  autoridades: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_AUTORIDADES_SUCCESS': {
      return {
        ...state,
        autoridades: action.response,
      };
    }

    case 'RESET_AUTORIDADE': {
      return { ...state, loading: false, success: false }
    }

    case 'CREATE_CHAMADO_REQUEST': {
      return {
        ...state,
        loading: true,
        success: true,
      };
    }

    case 'CREATE_CHAMADO_REQUEST_SUCCESS': {
      return {
        ...state,
        loading: false,
        success: true,
      };
    }

    default: {
      return state;
    }
  }
};
