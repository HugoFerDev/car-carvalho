const initialState = {
  patios: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_PATIOS_SUCCESS': {
      return {
        ...state,
        patios: action.response,
      };
    }

    default: {
      return state;
    }
  }
};
