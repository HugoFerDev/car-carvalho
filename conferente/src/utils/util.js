
export default class Util {
  /**
   * Método responsável por ler um arquivo e retorma uma Promise do arquivo.
   *
   * @param {*} inputFile
   */
  static readUploadedFileAsText(inputFile) {
    const temporaryFileReader = new FileReader();

    return new Promise((resolve, reject) => {
      temporaryFileReader.onerror = () => {
        temporaryFileReader.abort();
        reject();
      };

      if (inputFile) {
        temporaryFileReader.readAsDataURL(inputFile);
        temporaryFileReader.onload = () => {
          this.compressImage(temporaryFileReader.result).then((data) => {
            resolve(data);
          });
        };
      }
    });
  }

  static compressImage(base64) {
    const canvas = document.createElement('canvas');
    const img = document.createElement('img');

    return new Promise((resolve, reject) => {
      img.onload = () => {
        let { width, height } = img;
        const maxHeight = 1024;
        const maxWidth = 768;

        if (width > height) {
          if (width > maxWidth) {
            height = Math.round((height *= maxWidth / width));
            width = maxWidth;
          }
        } else if (height > maxHeight) {
          width = Math.round((width *= maxHeight / height));
          height = maxHeight;
        }
        canvas.width = width;
        canvas.height = height;

        const ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, width, height);

        resolve(canvas.toDataURL('image/jpeg', 1.0));
      };
      img.onerror = (err) => {
        reject(err);
      };
      img.src = base64;
    });
  }

  /**
   * Método responsável por gerar um número do OS do checklist.
   */
  static generateOS() {
    let randomString = '';
    let randomAscii;
    const asciiLow = 65;
    const asciiHigh = 90;
    for (let i = 0; i < 2; i++) {
      randomAscii = Math.floor((Math.random() * (asciiHigh - asciiLow)) + asciiLow);
      randomString += String.fromCharCode(randomAscii);
    }

    randomString += Math.floor(Math.random() * (9 - 0 + 1) + 0);
    randomString += Math.floor(Math.random() * (9 - 0 + 1) + 0);
    randomString += Math.floor(Math.random() * (9 - 0 + 1) + 0);
    randomAscii = Math.floor((Math.random() * (asciiHigh - asciiLow)) + asciiLow);
    randomString += String.fromCharCode(randomAscii);

    return randomString;
  }

  static formatDate(date) {
    let value = date.split('T')[0].split('-');
    return `${value[2]}/${value[1]}/${value[0]}`
  }
}
